#include <iostream>
#include <cmath>
#include "print.h"
#include "operators.h"
#include "control_flow.h"

const double sqrtOf2 = std::sqrt(2);

using namespace std;

constexpr int triang(int i) {
    return (i == 0) ? 0 : triang(i - 1) + i;
}

// enumerations
enum suits {clubs, diamonds, hearts, spades};
enum class suitsScoped : char {clubs, diamonds, hearts, spades};
enum ports {ftp=21, ssh, telnet, smtp=25, http=80};

// namespaces
namespace utilities {
    bool pool_data() {
        return true;
    }
    int get_data() {
        return 42;
    }
}

namespace util {
    // declare the functions
    bool pool_data();
    int get_data();
}
// define the functions
bool util::pool_data() {
    return false;
}
int util::get_data() {
    return -42;
}

namespace utils {
    inline namespace V1 {
        bool pool_data();
        int get_data();
    }
    namespace V2 {
        bool pool_data();
        int get_data();
        int new_feature();
    }
}
bool utils::pool_data() {
    return false;
}
int utils::get_data() {
    return -42;
}
bool utils::V2::pool_data() {
    return true;
}
int utils::V2::get_data() {
    return 42;
}
int utils::V2::new_feature() {
    return 5;
}

int inc(int i) {
    static int value;
    value += i;
    return value;
}

int main() {
    int i = 4;
    i = i / 2;
    cout << "The result is " << i << endl;
    // same as one-liner
    int j=4;j=j/2;cout<<"The result is "<<j<<endl;

    // comma operator
    int a = 9;
    int b = 4;
    int c;
    c = a + 8, b + 1;
    cout << "c: " << c << endl;
    // change operator precedence
    c = (a + 8, b + 1);
    cout << "c: " << c << endl;

    // variable initialisation
    int k = 1;
    int l = int(2);
    int m(3);
    auto n = 4; // compiler will infer type automatically
    cout << "k: " << k << ", l: " << l << ", m: " << m << ", n; " << n << endl;

    // literals
    int pos = +1;
    int neg = -1;
    double micro = 1e-6;
    double unit = 1.;
    string name = "Edu";
    cout << pos << ", " << neg << ", " << micro << ", " << unit << ", " << name << endl;

    // constants
    const double pi = 3.1415;
    double radius = 5.0;
    double circumference = 2 * pi * radius;
    cout << circumference << endl;

    cout << sqrtOf2 << endl;
    cout << endl;

    const int size = 5;
    int values[size];

    // constant expressions
    constexpr double myPi = 3.1415;
    constexpr double twopi = 2 * myPi;
    cout << twopi << endl;
    cout << triang(5) << endl;

    // enumerations
    suits card1 = diamonds;
    suits card2 = suits::hearts;
    cout << card1 << ", " << card2 << endl;
    // with scoped enum use of namespace is mandatory
    suitsScoped card3 = suitsScoped::clubs;

    cout << ports::ftp << endl;
    cout << ports::ssh << endl;

    // pointers
    int *p;
    int d = 42;
    p = &d;
    cout << p << endl;
    cout << *p << endl;

    int* p1, p2;
    p1 = &d;
    p2 = d; // p2 is int, not int*
    cout << p1 << endl;
    cout << *p1 << endl;
    cout << p2 << endl;
    int *p3, *p4;
    p3 = &d;
    p4 = &d;
    cout << p3 << endl;
    cout << p4 << endl;
    cout << endl;

    // namespaces
    if (utilities::pool_data()) {
        cout << "pooled data: " << utilities::get_data() << endl;
    } else {
        cout << "no data to pool" << endl;
    }

    if (util::pool_data()) {
        cout << "pooled data: " << util::get_data() << endl;
    } else {
        cout << "no data to pool" << endl;
    }

    if (utils::pool_data()) {
        cout << "pooled data: " << utils::get_data() << endl;
    } else {
        cout << "no data to pool" << endl;
    }

    if (utils::V1::pool_data()) {
        cout << "pooled data: " << utils::V1::get_data() << endl;
    } else {
        cout << "no data to pool" << endl;
    }

    if (utils::V2::pool_data()) {
        cout << "pooled data: " << utils::V2::get_data() << endl;
        cout << "new feature: " << utils::V2::new_feature() << endl;
    } else {
        cout << "no data to pool" << endl;
    }

    // namespace alias
    namespace u = utilities;
    cout << u::get_data() << endl;
    usage();

    cout << inc(10) << endl;
    cout << inc(5) << endl;

    operators_sample();
    control_flow();
    return 0;
}
