//
// Created by Eduard Luhtonen on 24.07.20.
//
#include <iostream>
#include <vector>
#include "control_flow.h"

using namespace std;

void check_range(int i) {
    if (i > 10) {
        cout << "much to high!" << endl;
    } else {
        cout << "within range" << endl;
    }
    if (-1 == i) {
        cout << "typed -1" << endl;
    }
    cout << "i = " << i << endl;
}

void numbers(int number) {
    cout << "there "
         << ((number == 1) ? "is " : "are ")
         << number << " item"
         << ((number == 1) ? "" : "s")
         << endl;
}

void switch_sample(int i) {
    switch (i) {
        case 1:
            cout << "one" << endl;
            break;
        case 2:
            cout << "two" << endl;
            break;
        default:
            cout << "other" << endl;
    }
}

void switch_sample(char c) {
    switch (c) {
        case 'a':
            cout << "character a" << endl;
            break;
        case 'z':
            cout << "character z" << endl;
            break;
        default:
            cout << "other character" << endl;
    }
}

enum suits {
    clubs, diamonds, hearts, spades
};

void print_name(suits card) {
    switch (card) {
        case suits::clubs:
            cout << "card is a club" << endl;
            break;
        default:
            cout << "car is not a club" << endl;
    }
}

void control_flow() {
    cout << endl;
    check_range(11);
    check_range(-1);

    int a = 2;
    int b = 3;
    int max;
    if (a > b) {
        max = a;
    } else {
        max = b;
    }
    cout << "max: " << max << endl;
    max = (a > b) ? a : b;
    cout << "max: " << max << endl;

    numbers(1);
    numbers(5);

    int i = 10, j = 0;
    ((i < j) ? i : j) = 7;
    cout << "i = " << i << ", j = " << j << endl;
    i = 0, j = 10;
    ((i < j) ? i : j) = 7;
    cout << "i = " << i << ", j = " << j << endl;

    switch_sample(1);
    switch_sample(2);
    switch_sample(12);
    switch_sample('a');
    switch_sample('z');
    switch_sample('e');

    print_name(suits::diamonds);
    print_name(suits::clubs);

    for (int i = 0; i < 10; ++i) {
        cout << i;
    }
    cout << endl;

    for (int i = 0;; ++i) {
        cout << i;
        if (i == 10) break;
    }
    cout << endl;

    for (float divisor = 0.f; divisor < 10.f; ++divisor) {
        cout << divisor;
        if (divisor == 0) {
            cout << endl;
            continue;
        }
        cout << " " << (1 / divisor) << endl;
    }

    vector<string> beatles = {"John", "Paul", "George", "Ringo"};
    for (int i = 0; i < beatles.size(); ++i) {
        cout << beatles.at(i) << endl;
    }
    for (string musician : beatles) {
        cout << musician << endl;
    }

    int birth_years[] = {1940, 1942, 1943, 1940};
    for (int birth_year : birth_years) {
        cout << birth_year << endl;
    }
    i = 5;
    while (i > 0) {
        cout << i-- << endl;
    }
    i = 5;
    do {
        cout << i-- << endl;
    } while (i > 0);

    // jumping
    for (int i = 0; i < 10; i++) {
        cout << i << endl;
        if (i == 5) goto end;
    }
    end:
    cout << "end" << endl;
}
