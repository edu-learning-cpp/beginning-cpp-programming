//
// Created by Eduard Luhtonen on 24.07.20.
//
#include <iostream>
#include "operators.h"

using std::cout;
using std::endl;

void operators_sample() {
    cout << endl;
    int b = 1;
    int c = 2;
    int d = 3;

    int a = b + c * d;
    cout << "a = b + c * d => " << a << endl;
    a = b + (c * d);
    cout << "a = b + (c * d) => " << a << endl;
    a = (b + c) * d;
    cout << "a = (b + c) * d => " << a << endl;
    a = b + c + d;
    cout << "a = b + c + d => " << a << endl;

    // arithmetic operators
    cout << (a / b) * b + (a % b) << endl;
    int height = 480;
    int width = 640;
    float aspect_ratio = width / height;
    cout << "Height: " << height << ", width: " << width << ", aspect ratio: " << aspect_ratio << endl;

    b = 5;
    cout << "b: " << b << endl;
    a = ++b;
    cout << "a: " << a << ", b: " << b << endl;
    b = 5;
    cout << "b: " << b << endl;
    a = b++;
    cout << "a: " << a << ", b: " << b << endl;
    int t;
    a = (t = b, b = b + 1, t);
    cout << "a: " << a << ", b: " << b << endl;

    // bitwise operators
    unsigned int x = 0x0a0a; // this is the binary 0000101000001010
    unsigned int y = 0x00ff; // this is the binary 0000000000001111
    unsigned int z = x & y;  // this is the binary 0000000000001010
    cout << std::hex << std::showbase << z << endl;
    z = x | y;  // this is the binary 0000000000001111
    cout << std::hex << std::showbase << z << endl;

    unsigned int flags = 0x0a0a; // 0000101000001010
    unsigned int test = 0x00ff;  // 0000000000001111

    // 0000101000001111 is (flags & test)
    if ((flags & test) == flags) {
        // code for when all the flags bits are set in test
        cout << "code for when all the flags bits are set in test" << endl;
    }
    if ((flags & test) != 0) {
        // code for when some or all the flag bits are set in test
        cout << "code for when some or all the flag bits are set in test" << endl;
    }

    int value = 0xf1;
    int flag = 0x02;
    int result = value ^ flag; // 0xf3
    cout << std::hex << result << endl;

    // boolean operators
    double w = 1.000001 * 1000000000000;
    double v = 1000001000000;
    if (w == v) {
        cout << "numbers are the same" << endl;
    }
    a = 10;
    b = 11;
    bool val = (x > y);
    if (val) {
        cout << "numbers same";
    } else {
        cout << "numbers not same";
    }
    cout << endl;
    if (!val) {
        cout << "numbers not same";
    } else {
        cout << "numbers same";
    }
    cout << endl;
    x = y = z = 10;
    if ((x == y) || (y < z)) {
        cout << "one or both are true" << endl;
    }
    if ((x != 0) && (0.5 > 1 / x)) {
        cout << "reciprocal is less than 0.5" << endl;
    }

    // bitwise shift
    unsigned short s1 = 0x0010;
    unsigned short s2 = s1 << 8;
    cout << std::hex << std::showbase;
    cout << s2 << endl;
    // 0x1000
    s2 = s2 << 3;
    cout << s2 << endl;
    s2 = s2 << 1;
    cout << s2 << endl;

    int i = 10;
    cout << std::dec;
    cout << "i = " << i << endl;
    i += 10;
    cout << "i = " << i << endl;
    i *= 8;
    cout << "i = " << i << endl;
    i <<= 3;
    cout << "i = " << i << endl;
}
