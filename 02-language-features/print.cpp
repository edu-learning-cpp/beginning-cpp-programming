//
// Created by Eduard Luhtonen on 24.07.20.
//

#include "print.h"
std::string app_name = "My Utility";
void print_version() {
    std::cout << "Version = " << ::version << std::endl;
}

void usage() {
    std::cout << app_name << " ";
    print_version();
}
