//
// Created by Eduard Luhtonen on 01.08.20.
//
#include <iostream>
#include "using_pointers.h"

using namespace std;

// poorly written function that returns a string allocated on the stack in a function
char *get() {
    char c[]{"hello"};
    return c;
}

void using_pointers() {
    int arr[]{1, 2, 3, 4};
    for (int i = 0; i < 4; ++i) {
        arr[i] += arr[i + 1]; // oops, what happens when i == 3?
        cout << arr[i] << " ";
    }
    cout << endl;

    char c[]{"hello"}; // c can be used as a pointer
    *c = 'H';             // OK, can write thru the pointer
    const char *ptc{c};  // pointer to constant
    cout << ptc << endl;  // OK, can read the memory pointed to
    // *ptc = 'Y';          // cannot write to the memory
    char *const cp{c};   // constant pointer
    *cp = 'y';            // can write thru the pointer
    // cp++;                 // cannot point to anything else
    cout << cp << endl;
    cout << endl;

    char c1[]{"hello"};
    cout << c1 << endl;
    char *const cp1{c1}; // cannot point to any other memory
    *cp1 = 'H';            // can change the memory
    cout << "pointer 1: " << cp1 << endl;
    const char *ptc1 = const_cast<const char *>(cp1);
    ptc1++;                 // change where the pointer points to
    char *const cp2 = const_cast<char *const>(ptc1);
    *cp2 = 'a';            // now points to Hallo
    cout << "pointer 2: " << cp2 << endl;
    cout << c1 << endl;
    cout << endl;

    int *pi = static_cast<int *>(malloc(sizeof(int)));
    *pi = 42;
    cout << *pi << endl;
    free(pi);
    cout << endl;

    wchar_t wc[]{L"hello"};
    wcout << wc << " is stored in memory at ";
    wcout << hex;
    wcout << reinterpret_cast<int *>(wc) << endl;
    wcout << &wc << endl;
    wcout << "The characters are:" << endl;
    short *ps = reinterpret_cast<short *>(wc);
    do {
        wcout << *ps << endl;
    } while (*ps++);
}
