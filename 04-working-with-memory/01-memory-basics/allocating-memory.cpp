//
// Created by Eduard Luhtonen on 01.08.20.
//
#include <iostream>
#include "allocating-memory.h"

using namespace std;

void allocate_arrays() {
    int *p = new int[2];
    p[0] = 1;
    *(p + 1) = 2;
    for (int i = 0; i < 2; ++i) cout << p[i] << endl;
    delete[] p;
    cout << endl;
}

void handle_failed_allocation(long long VERY_BIG_NUMBER) {
    // VERY_BIG_NUMBER is a constant defined elsewhere
    int *pi;
    try {
        pi = new int[VERY_BIG_NUMBER];
        // other code
        cout << pi << endl;
    } catch (const std::bad_alloc &e) {
        cout << "cannot allocate" << endl;
        return;
    }
    // use pointer
    delete[] pi;
}

void handle_failure(long long VERY_BIG_NUMBER) {
    int *pi = new(std::nothrow) int[VERY_BIG_NUMBER];
    if (nullptr == pi) {
        cout << "cannot allocate" << endl;
    } else {
        // use pointer
        cout << pi << endl;
        delete[] pi;
    }
    cout << endl;
}

int *do_something(int *p) {
    *p *= 10;
    return p;
}

void memory_lifetime() {
    int *p1 = new int(42);
    cout << *p1 << endl;
    int *p2 = do_something(p1);
    cout << *p2 << endl;
    delete p1;
    p1 = nullptr;
    cout << p1 << endl;
    // what about p2?
    cout << p2 << endl;
    cout << *p2 << endl;
    cout << endl;
}

void allocate_memory() {
    cout << endl;
    int *p = new int; // allocate memory for one int
    cout << p << endl;
    delete p;

    int *p1 = new int(42);
    int *p2 = new int{42};
    cout << p1 << endl;
    cout << *p1 << endl;
    cout << p2 << endl;
    cout << *p2 << endl;
    cout << endl;

    allocate_arrays();
    handle_failed_allocation(-1);
    handle_failure(-1);
    memory_lifetime();
}
