//
// Created by Eduard Luhtonen on 01.08.20.
//
#include <iostream>
#include <array>
#include "memory-and-stl.h"

using namespace std;

void use_ten_ints(int* arr) {
    for (int i = 0; i < 10; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

void memory_and_stl() {
    array<int, 4> arr { 1, 2, 3, 4 };
    for (int i : arr) cout << i << " ";
    cout << endl;
    for (int i = 0; i < arr.size(); ++i) cout << arr[i] << " ";
    cout << endl;

    int arr1[] { 1, 2, 3, 4 };
    use_ten_ints(arr1); // oops will read past the end of the buffer

    array<int, 4> arr2 { 1, 2, 3, 4 };
    // use_ten_ints(arr2); // will not compile
    use_ten_ints(&arr2[0]);    // compiles, but on your head be it
    use_ten_ints(arr2.data()); // ditto

    array<int, 4> arr3;
    arr3.fill(42);   // put 42 in each item
    for (auto v : arr3) cout << v << " ";
    cout << endl;
    arr2.swap(arr3); // swap items in arr2 with items in arr3
    for (auto v : arr2) cout << v << " ";
    cout << endl;
    cout << endl;
}
