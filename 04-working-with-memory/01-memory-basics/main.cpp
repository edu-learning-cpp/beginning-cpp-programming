#include "using-memory.h"
#include "using_arrays.h"
#include "using_pointers.h"
#include "allocating-memory.h"
#include "memory-and-stl.h"
#include "references.h"

int main() {
    using_memory();
    using_arrays();
    using_pointers();
    allocate_memory();
    memory_and_stl();
    references();
    return 0;
}
