//
// Created by Eduard Luhtonen on 31.07.20.
//
#include <iostream>
#include "using-memory.h"

using namespace std;

void using_null_pointers() {
    int *pi = nullptr;
    int i = 42;
    pi = &i;
    if (nullptr != pi) cout << *pi << endl;
}

void types_of_memory() {
    char const *p1{"hello"};
    char const *p2{"hello"};
    cout << p1 << endl;
    cout << p2 << endl;
    cout << &p1 << endl;
    cout << &p2 << endl;
}

void my_print(int v[]) {
    for (int i = 0; i < 5; i++) {
        cout << v[i] << " ";
    }
    cout << endl;
}

void pointer_arithmetic() {
    int v[]{1, 2, 3, 4, 5};
    my_print(v);
    int *pv = v;
    *pv = 11;
    my_print(v);
    v[1] = 12;
    pv[2] = 13;
    my_print(v);
    *(pv + 3) = 14;
    my_print(v);
    cout << endl;

    int v1[]{1, 2, 3, 4, 5};
    int *pv1 = v1;
    my_print(v1);
    for (int i = 0; i < 5; ++i) {
        *pv1++ *= 3;
    }
    my_print(v1);
}

void using_memory() {
    int i = 42;
    cout << i << endl;
    int *pi = &i;
    *pi = 99;
    cout << i << endl;

    using_null_pointers();
    cout << endl;
    types_of_memory();
    cout << endl;
    pointer_arithmetic();
    cout << endl;
}
