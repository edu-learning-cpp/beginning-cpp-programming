//
// Created by Eduard Luhtonen on 31.07.20.
//
#include <iostream>
#include <cstring>
#include "using_arrays.h"

using namespace std;

template<int N>
void print_array(int (&arr)[N]) {
    for (int x : arr) {
        cout << x << " ";
    }
    cout << endl;
}

void using_arrays() {
    int squares[4];
    for (int i = 0; i < 4; ++i) {
        squares[i] = i * i;
    }
    print_array(squares);

    // array size and initialisation loop mismatch
    int squares1[5];
    for (int i = 0; i < 4; ++i) {
        squares1[i] = i * i;
    }
    print_array(squares1);

    // to solve the issue above
    constexpr int sq_size = 4; // array declaration must have a constant size
    int squares2[sq_size];
    for (int i = 0; i < sq_size; ++i) {
        squares2[i] = i * i;
    }
    print_array(squares2);

    int squares3[4];
    for (int i = 0; i < sizeof(squares3) / sizeof(squares3[0]); ++i) {
        squares3[i] = i * i;
    }
    print_array(squares3);
    cout << endl;

    // multidimensional arrays
    int four_by_three[4][3] = {11, 12, 13, 21, 22, 23, 31, 32, 33, 41, 42, 43};
    cout << four_by_three[3][1] << endl;
    int four_by_three2[4][3] = {{11, 12, 13},
                                {21, 22, 23},
                                {31, 32, 33},
                                {41, 42, 43}};
    cout << four_by_three2[3][1] << endl;
    int four_by_three3[4][3] = {{11, 12, 13},
                                {},
                                {31, 32, 33},
                                {41, 42, 43}};
    cout << four_by_three3[3][1] << endl;
    int four_by_three_by_two[4][3][2]
            = {{{111, 112}, {121, 122}, {131, 132}},
               {{211, 212}, {221, 222}, {231, 232}},
               {{311, 312}, {321, 322}, {331, 332}},
               {{411, 412}, {421, 422}, {431, 432}}
            };
    cout << four_by_three_by_two[3][2][0];
    cout << endl;

    // arrays of characters
    char p1[6];
    strncpy(p1, "hello", 6);
    char p2[6];
    strncpy(p2, p1, 6);
    bool b = (p1 == p2);
    cout << p1 << endl;
    cout << p2 << endl;
    cout << b << endl;
    string s1("string");
    string s2("string");
    int result = s1.compare(s2);
    cout << s1 << endl;
    cout << s2 << endl;
    cout << (result == 0) << endl;
    if (s1 == s2) {
        cout << "strings are the same" << endl;
    }
    cout << endl;

    char pHello[5];          // enough space for 5 characters
    // strcpy(pHello, "hello"); // this causes buffer overrun
    strncpy(pHello, "hello", 5);
    cout << pHello << endl;
    cout << endl;
}
