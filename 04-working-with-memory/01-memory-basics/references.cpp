//
// Created by Eduard Luhtonen on 01.08.20.
//
#include <iostream>
#include "references.h"

using namespace std;

string &hello() {
    string str("hello");
    return str; // don't do this!
} // str no longer exists at this point

void use_string(const string &csr) {
    cout << csr << endl;
}

string global{"global"};

string &get_global() {
    return global;
}

string &get_static() {
    static string str{"static"};
    return str;
}

string get_temp() {
    return "temp";
}

void use_string(string &rs) {
    string s{rs};
    for (size_t i = 0; i < s.length(); ++i) {
        if ('a' == s[i] || 'b' == s[i] || 'o' == s[i])
            s[i] = '_';
    }
    cout << s << endl;
}

void use_string(string &&s) {
    for (size_t i = 0; i < s.length(); ++i) {
        if ('a' == s[i] || 'b' == s[i] || 'o' == s[i]) s[i] = '_';
    }
    cout << s << endl;
}

void references() {
    int i = 42;
    int *pi = &i;  // pointer to an integer
    cout << "pointer: " << pi << endl;
    cout << "value: " << *pi << endl;
    int &ri1 = i;  // reference to a variable
    cout << ri1 << endl;
    i = 99;        // change the integer thru the variable
    cout << ri1 << endl;
    *pi = 101;     // change the integer thru the pointer
    cout << "pointer: " << pi << endl;
    cout << "value: " << *pi << endl;
    ri1 = -1;      // change the integer thru the reference
    cout << ri1 << endl;
    int &ri2{i};  // another reference to the variable
    cout << ri2 << endl;
    int j = 1000;
    pi = &j;       // point to another integer
    cout << "pointer: " << pi << endl;
    cout << "value: " << *pi << endl;
    cout << endl;

    // int& r1;           // error, must refer to a variable
    // int& r2 = nullptr; // error, must refer to a variable

    int x = 1, y = 2;
    cout << "x = " << x << endl;
    cout << "y = " << y << endl;
    int &rx = x; // declaration, means rx is an alias for x
    rx = y;      // assignment, changes value of x to the value of y
    cout << "x = " << x << endl;
    cout << "y = " << y << endl;

    // constant references
    i = 42;
    const int &ri = i;
    cout << ri << endl;
//    ri = 99;           // error!
    cout << endl;

    // returning references
    cout << "result from hello(): '" << hello() << "'" << endl;
    cout << endl;

    // temporaries and references
    const int &cri{42};
    cout << cri << endl;
    string str{"hello"};
    use_string(str);      // a std::string object
    const char *cstr = "hello";
    use_string(cstr);     // a C string can be converted to a std::string
    use_string("hello");  // a literal can be converted to a std::string
    cout << endl;

    cout << get_global() << endl;
    cout << get_static() << endl;
    cout << get_temp() << endl;
    cout << endl;

    use_string(get_global()); // string&  version
    use_string(get_static()); // string&  version
    use_string(get_temp());   // string&& version
    use_string("C string");   // string&& version
    string str1{"C++ string"};
    use_string(str1);          // string&  version
    cout << endl;

    // ranged for and references
    constexpr int size = 4;
    int squares[size];

    for (int i = 0; i < size; ++i) {
        squares[i] = i * i;
    }
    for (int j : squares) {
        cout << j << " ";
    }
    cout << endl;
    for (int &k : squares) {
        k *= 2;
    }
    for (int j : squares) {
        cout << j << " ";
    }
    cout << endl;
    cout << endl;

    int arr[2][3] { { 2, 3, 4 }, { 5, 6, 7} };
    for (auto& row : arr) {
        for (auto col : row) { // will not compile
            cout << col << " ";
        }
        cout << endl;
    }
}
