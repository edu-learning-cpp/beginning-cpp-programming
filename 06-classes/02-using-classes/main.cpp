#include <iostream>
#include <dirent.h>
#include <regex>

using namespace std;

class file_search {
    class search_handle {
        DIR *handle;
    public:
        search_handle() {handle = nullptr;}
        search_handle(DIR *p) {handle = p;}
        void operator=(DIR *p) { handle = p; }
        // do not allow copying
        search_handle(search_handle& h) = delete;
        void operator=(search_handle& h) = delete;
        // move semantics allowed
        search_handle(search_handle&& h) { close(); handle = h.handle; }
        void operator=(search_handle&& h) { close(); handle = h.handle; }
        // allow implicit conversion to a bool or check that the
        // data member is valid
        operator bool() const { return (handle != nullptr); }
        // allow implicit conversion to an DIR
        DIR* operator() () const { return handle ; }
        void close() { if (handle != nullptr) closedir(handle); handle = nullptr; }
        ~search_handle() { close(); }
    };

    search_handle handle;
    string search;
public:
    file_search(const char* str) : search(str) {}
    file_search(const string& str) : search(str) {}
    const char* path() const { return search.c_str(); }
    void close() { handle.close(); }
    bool next(string& ret) {
        dirent *find;
        if (!handle) {
            // the first call to get search handle
            handle = opendir(".");
            if (!handle) return false;
        }
        while(nullptr != (find = readdir(handle()))) {
            regex rgx(search);
            cmatch match;
            if (regex_search(find->d_name, match, rgx)) {
                ret = find->d_name;
                return true;
            }
        }
        return false;
    }
};

void usage() {
    cout << "usage: search pattern" << endl;
    cout << "pattern is the file or folder to search for "
         << "without wildcards * and ?" << endl;
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        usage();
        return 1;
    }

    file_search files(argv[1]);
    cout << "searching for " << files.path() << endl;
    string file;
    while (files.next(file)) {
        cout << file << endl;
    }

    return 0;
}
