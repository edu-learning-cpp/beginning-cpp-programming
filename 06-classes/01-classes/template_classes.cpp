//
// Created by Eduard Luhtonen on 05.08.20.
//
#include <iostream>
#include <string>
#include "template_classes.h"

using namespace std;

template <int N, typename T> class simple_array {
    T data[N];
public:
    const T* begin() const { return data; }
    const T* end() const { return data + N; }
    int size() const { return N; }

    T& operator[](int idx) {
        if (idx < 0 || idx >= N)
            throw range_error("Range 0 to " + to_string(N));
        return data[idx];
    }
};

template<int N> class simple_array<N, char> {
    char data[N];
public:
    simple_array<N, char>(const char* str) {
        strncpy(data, str, N);
    }
    int size() const { return N; }
    char& operator[](int idx) {
        if (idx < 0 || idx >= N)
            throw range_error("Range 0 to " + to_string(N));
        return data[idx];
    }
    operator const char*() const { return data; }
};

template<> class simple_array<256, char> {
    char data[256];
public:
    // etc
};

void template_classes() {
    simple_array<4, int> four;
    four[0] = 10; four[1] = 20; four[2] = 30; four[3] = 40;
    for(int i : four) cout << i << " "; // 10 20 30 40
    cout << endl;
    four[4] = -99;            // throws a range_error exception
}
