//
// Created by Eduard Luhtonen on 05.08.20.
//
#include <iostream>
#include <cmath>
#include <vector>
#include <functional>
#include "objects_with_pointers.h"

using namespace std;

struct point_s {
    double x;
    double y;
};

ostream &operator<<(ostream &stm, const point_s &pt) {
    stm << "(" << pt.x << "," << pt.y << ")";
    return stm;
}

class cartesian_vector;

class point2 {
    double x;
    double y;
public:
    point2() {
        x = 0;
        y = 0;
    }

    point2(double x, double y) : x(x), y(y) {}

    double get_x() const { return x; }
    void set_x(double val) { x = val; }

    double get_y() const { return y; }
    void set_y(double val) { y = val; }

    point2 operator-() const {
        return point2(-this->x, -this->y);
    }

    friend class cartesian_vector;
};

class cartesian_vector {
public:
    double x;
    double y;

    cartesian_vector(double a, double b) {
        x = a;
        y = b;
    }

    // other items
    double get_magnitude() const {
        return sqrt((this->x * this->x) + (this->y * this->y));
    }

    cartesian_vector operator-(point2 &rhs) const {
        return cartesian_vector(this->x - rhs.x, this->y - rhs.y);
    }
};

ostream &operator<<(ostream &stm, const point2 &pt) {
    stm << "(" << pt.get_x() << "," << pt.get_y() << ")";
    return stm;
}

class mytype {
    int i;
public:
    mytype &operator++() {
        // do actual increment
        return *this;
    }

    mytype operator++(int) {
        mytype tmp(*this);
        operator++(); // call the prefix code
        return tmp;
    }
    mytype(int i) : i(i) {}
    explicit mytype(string s) : i(s.size()) {}
    operator int () const { return i; }
};

// defining function classes
class factor {
    double f = 1.0;
public:
    factor(double d) : f(d) {}

    double operator()(double x) const { return f * x; }
};

template<typename Fn>
void print_value(double d, Fn &fn) {
    double ret = fn(d);
    cout << ret << endl;
}

template<typename Fn>
int compare_vals(vector<double> d1, vector<double> d2, Fn compare) {
    if (d1.size() > d2.size()) return -1; // error
    int c = 0;
    for (size_t i = 0; i < d1.size(); ++i) {
        if (compare(d1[i], d2[i])) c++;
    }
    return c;
}

template<typename Fn>
int compare_vals(vector<double> d, Fn compare){
    int c = 0;
    for (size_t i = 0; i < d.size(); ++i) {
        if (compare(d[i])) c++;
    }
    return c;
}

class averager {
    double total;
    int count;
public:
    averager() : total(0), count(0) {}
    void operator()(double d) { total += d; count += 1; }
    operator double() const {
        return (count != 0) ? (total / count) :
               numeric_limits<double>::signaling_NaN();
    }
};

// The following allocates an object on the free store and manually maintains its lifetime
void f1() {
    int* p = new int;
    *p = 42;
    cout << *p << endl;
    delete p;
}

// same code with a smart pointer
void f2() {
    unique_ptr<int> p(new int);
    *p = 42;
    cout << *p << endl;
    delete p.release();
}

// another way to deallocate the resource
void f3() {
    unique_ptr<int> p(new int);
    *p = 42;
    cout << *p << endl;
    p.reset();
}

template<typename T> struct my_deleter {
    void operator()(T* ptr) {
        cout << "deleted the object!" << endl;
        delete ptr;
    }
};
void f3_1() {
    unique_ptr<int, my_deleter<int>> p(new int);
    *p = 42;
    cout << *p << endl;
    p.reset();
}

// allow the smart pointer to manage the resource lifetime
// by allowing the smart pointer object to go out of scope
void f4() {
    unique_ptr<int> p(new int);
    *p = 42;
    cout << *p << endl;
} // memory is deleted

// the preferred way to create smart pointers based on this class
void f5() {
    unique_ptr<int> p = make_unique<int>();
    *p = 42;
    cout << *p << endl;
} // memory is deleted

void f6() {
    unique_ptr<point2> p = make_unique<point2>(1.0, 1.0);
    p->set_x(42);
    cout << p->get_x() << "," << p->get_y() << endl;
} // memory is deleted

void using_objects_with_pointers() {
    point_s p{10.0, 10.0};
    cout << p << endl;
    double *pp = &p.x;
    cout << *pp << endl;
    cout << endl;

    double (cartesian_vector::*fn)() const = nullptr;
    fn = &cartesian_vector::get_magnitude;

    cartesian_vector vec(1.0, 1.0);
    double mag = (vec.*fn)();

    cartesian_vector *pvec = new cartesian_vector(1.0, 1.0);
    double mag2 = (pvec->*fn)();
    delete pvec;

    point2 p1(-1, 1);
    point2 p2 = -p1; // p2 is (1,-1)
    cout << p1 << endl;
    cout << p2 << endl;
    cout << endl;

    factor threeTimes(3);        // create the functor object
    double ten = 10.0;
    double d1 = threeTimes(ten); // calls operator(double)
    double d2 = threeTimes(d1);  // calls operator(double)
    cout << d1 << endl;
    cout << d2 << endl;

    double d3 = threeTimes.operator()(d1);
    cout << d3 << endl;
    cout << endl;

    vector<double> vec1{ 1.0, 2.0, 3.0, 4.0 };
    vector<double> vec2{ 1.0, 1.0, 2.0, 5.0 };
    int c = compare_vals(vec1, vec2, greater<double>());
    cout << c << endl;
    using namespace::std::placeholders;
    c = compare_vals(vec1, bind(greater<double>(), _1, 2.0));
    cout << c << endl;
    c = compare_vals(vec1, bind(greater<double>(), 2.0, _1));
    cout << c << endl;
    cout << endl;

    // defining conversion operators
    string s = "hello";
    mytype t = mytype(s); // explicit conversion
    int i = t;            // implicit conversion
    cout << "mytype: " << t.operator int() << endl;
    cout << "i: " << i << endl;
    cout << endl;

    vector<double> vals { 100.0, 20.0, 30.0 };
    double avg = for_each(vals.begin(), vals.end(), averager());
    cout << avg << endl;
    cout << endl;

    f1();
    f2();
    f3();
    f3_1();
    f4();
    f5();
    f6();
    cout << endl;

    // Sharing ownership
    point2* pt = new point2(1.0, 1.0);
    shared_ptr<point2> sp1(pt); // Important, do not use p after this!
    shared_ptr<point2> sp2(sp1);
    cout << *pt << endl;
    pt = nullptr;
    sp2->set_x(2.0);
    sp1->set_y(2.0);
    cout << *sp1 << endl;
    cout << *sp2 << endl;
    sp1.reset(); // get rid of one shared pointer

    // preferred way to creat shared pointer
    shared_ptr<point2> sp11 = make_shared<point2>(1.0,1.0);
    cout << endl;

    // Handling dangling pointers
    shared_ptr<point2> sp21 = make_shared<point2>(1.0,1.0);
    weak_ptr<point2> wp(sp21);

    // code that may call sp1.reset() or may not
    cout << *sp21 << endl;

    if (!wp.expired())  { /* can use the resource */}

    shared_ptr<point2> sp22 = wp.lock();
    if (sp22 != nullptr) {
        /* can use the resource */
        cout << *sp22 << endl;
    }

    try {
        shared_ptr<point2> sp3(wp);
        // use the pointer
    } catch(bad_weak_ptr& e) {
        // dangling weak pointer
    }
    cout << endl;
}
