//
// Created by Eduard Luhtonen on 04.08.20.
//
#include <iostream>
#include <cmath>
#include <array>
#include "writing_classes.h"

using namespace std;

class cartesian_vector2; // forward declaration

class point {
    double x;
    double y;
public:
    point() {
        x = 0;
        y = 0;
    }
    point(double x, double y) : x(x), y(y) {}
    point(const point& rhs) : x(rhs.x), y(rhs.y) {} // copy constructor
    double get_x() const { return x; }
    double get_y() const { return y; }
    friend class cartesian_vector2;
    friend ostream& operator<<(ostream&, const point&);
    static point polar(double r, double th) {
        return point(r * cos(th), r * sin(th));
    }
};

ostream& operator<<(ostream& stm, const point& pt){
    stm << "(" << pt.x << "," << pt.y << ")";
    return stm;
}

class cartesian_vector {
public:
    double x;
    double y;
    // other methods
    double get_magnitude() const {
        return sqrt((x * x) + (y * y));
    }
    void reset(double x, double y) {
        this->x = x;
        this->y = y;
    }
    double magnitude() const;
};

double cartesian_vector::magnitude() const {
    return sqrt((this->x * this->x) + (this->y * this->y));
}

class cartesian_vector2 {
    double x;
    double y;
public:
    double get_x() { return this->x; }
    double get_y() { return this->y; }
    void set_x(double d);
    void set_y(double d);
    double magnitude() const {
        return sqrt((this->x * this->x) + (this->y * this->y));
    }
    cartesian_vector2() {}
    cartesian_vector2(const point& p) : x(p.x), y(p.y) {}
};

void cartesian_vector2::set_x(double d) {
    if (d > -100 && d < 100) this->x = d;
}

void cartesian_vector2::set_y(double d) {
    if (d > -100 && d < 100) this->y = d;
}

class point_unused {
public:
    //  direct initialization with default values
    int x = 0;
    int y = 0;
};

void print_point(point p) {
    cout << "point: " << p << endl;
}

class car {
    array<double, 4> tire_pressures;
    double spare;
public:
    car(double front, double back, double s = 25.0)
            : tire_pressures{front, front, back, back}, spare{s} {}
    car(double all) : car(all, all) {}
    void print_car();
};

void car::print_car() {
    cout << "car tire pressure: ";
    for (auto t : this->tire_pressures) {
        cout << t << ", ";
    }
    cout << "spare tire: " << this->spare << endl;
}

void print_cartesian_vector(cartesian_vector2 vec) {
    cout << "cartesian vector (" << vec.get_x() << "," << vec.get_y() << ") ";
    cout << "have length: " << vec.magnitude() << endl;
}

void copy_constructor() {
    point p1(10, 10);
    point p2(p1);       // copy constructor called
    point p3 = p1;      // copy constructor called
    print_point(p1);
    print_point(p2);
    print_point(p3);

    // converting between types
    point p(10, 10);
    cartesian_vector2 v1(p);
    print_cartesian_vector(v1);
    cartesian_vector2 v2 { p };
    print_cartesian_vector(v2);
    cartesian_vector2 v3 = p;
    print_cartesian_vector(v3);
}

class mytype {
public:
    mytype() {}
    explicit mytype(double x) {
        cout << "mytype explicit constructor " << x << endl;
    }
    static void f(){}
    void g(){ f(); }
    static int i;
    static void incr() { i++; }
};

// destructing objects
void f(mytype t) { // copy created
    // use t
}   // t destroyed

void g() {
    mytype t1;
    f(t1);
    if (true) {
        mytype t2;
    }   // t2 destroyed

    mytype arr[4];
}  // 4 objects in arr destroyed in reverse order to creation
// t1 destroyed

mytype get_object() {
    mytype t;               // default constructor creates t
    return t;               // copy constructor creates a temporary
}                           // t destroyed

void h() {
    mytype tt = get_object(); // copy constructor creates tt
}                             // temporary destroyed, tt destroyed

mytype *get_object_p() {
    return new mytype; // default constructor called
}

void f() {
    mytype *p = get_object_p();
    // use p
    delete p;        // object destroyed
}

class buffer {
    // data members
public:
    buffer() {}
    buffer(const buffer&) {}            // copy constructor
    buffer& operator=(const buffer&) {} // copy assignment
};

void assigning_objects() {
    buffer a, b, c;              // default constructors called
    // do something with them
    // a = b = c;                   // make them all the same value
    // a.operator=(b.operator=(c)); // make them all the same value
}

// move semantics
class mytype2 {
    int *p;
public:
    mytype2(const mytype2&) = delete;             // copy constructor
    mytype2& operator= (const mytype2&) = delete; // copy assignment
    mytype2(mytype2&&);                          // move constructor
    mytype2& operator=(mytype2&&);                // move assignment
};

mytype2::mytype2(mytype2&& tmp) {
    this->p = tmp.p;
    tmp.p = nullptr;
}

int mytype::i = 42;

void static_members() {
    mytype c;
    c.g();       // call the nonstatic method
    c.f();       // can also call the static method thru an object
    mytype::f(); // call static method without an object

    cout << mytype::i << endl;
}

void named_constructors() {
    const double pi = 3.141529;
    const double root2 = sqrt(2);
    point p11 = point::polar(root2, pi/4);
    cout << p11 << endl;
}

// nested classes
class outer {
public:
    class inner {
    public:
        void f();
    };
    inner g() { return inner(); }
};

void outer::inner::f() {
    // do something
}

void use_point(const point& p) {
    cout << "(" << p.get_x() << "," << p.get_y() << ")" << endl;
}

void writing_classes() {
    // object is created on the stack and initialized with an initializer list
    cartesian_vector vec{3.0, 4.0};
    cout << "cartesian vector (" << vec.x << "," << vec.y << ") ";
    double len = vec.get_magnitude();
    cout << "have length: " << len << endl;
    cout << vec.magnitude() << endl;
    cout << endl;

    cartesian_vector2 vec2{};
    vec2.set_x(3.0);
    vec2.set_x(2.0);
    cout << "cartesian vector2 (" << vec2.get_x() << "," << vec2.get_y() << ")" << endl;
    cout << endl;

    // object is created in the free store and initialized with an initializer list
    cartesian_vector *pvec = new cartesian_vector{5, 5};
    // use pvec
    cout << "cartesian vector (" << pvec->x << "," << pvec->y << ")" << endl;
    // object on the free store must be freed
    delete pvec;
    cout << endl;

    point p; // default constructor called
    print_point(p);
    point p1(); // compiles, but is a function prototype!
    // print_point(p1); // will not compile
    point p2{};  // calls default constructor
    print_point(p2);
    point p3(10.0, 10.0);
    print_point(p3);
    point arr[4];
    cout << "array:" << endl;
    for (auto point : arr) {
        print_point(point);
    }
    cout << endl;

    car commuter_car(25, 27);
    commuter_car.print_car();
    car sports_car(26, 28, 27);
    sports_car.print_car();
    car other_car(25);
    other_car.print_car();
    cout << endl;

    copy_constructor();
    cout << endl;

    //mytype t1 = 10.0; // will not compile, cannot convert
    mytype t2(10.0);  // OK
    cout << endl;

    assigning_objects();
    cout << endl;
    static_members();
    cout << endl;
    named_constructors();
    cout << endl;

    use_point(p3);
    cout << endl;
}
