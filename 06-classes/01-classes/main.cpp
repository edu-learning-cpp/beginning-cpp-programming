#include "writing_classes.h"
#include "objects_with_pointers.h"
#include "template_classes.h"

int main() {
    // Writing classes
    writing_classes();
    // Using objects with pointers
    using_objects_with_pointers();
    // Templates
    template_classes();
    return 0;
}
