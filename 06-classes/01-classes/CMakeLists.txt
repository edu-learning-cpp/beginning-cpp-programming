cmake_minimum_required(VERSION 3.17)
project(01_classes)

set(CMAKE_CXX_STANDARD 17)

add_executable(01_classes main.cpp writing_classes.cpp writing_classes.h objects_with_pointers.cpp objects_with_pointers.h template_classes.cpp template_classes.h)