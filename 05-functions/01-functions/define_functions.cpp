//
// Created by Eduard Luhtonen on 02.08.20.
//
#include <iostream>
#include "define_functions.h"

using std::cout, std::endl;

int mult(int, int);

inline auto mult2(int lhs, int rhs) -> int {
    return lhs * rhs;
}

// specifying exceptions
// next is deprecated since C++ 11 and is not allowed in C++ 17 or higher
//int calculate(int param) throw(std::overflow_error) {
//    // do something which potentially may overflow
//}

// C++11 style:
int increment(int param) noexcept {
    // check the parameter and handle overflow appropriately
}

void define_functions() {
    cout << mult(6, 7) << endl;
    cout << mult2(5, 6) << endl;
}

int mult(int a, int b) {
    return a * b;
}
