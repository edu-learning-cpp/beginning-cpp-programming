//
// Created by Eduard Luhtonen on 03.08.20.
//
#include <iostream>
#include <string>
#include "function_pointers.h"

using std::cout, std::endl;

// values over zero are error codes
int get_status() {
    int status = 0;
    // code that checks the state of data is valid
    return status;
}

using two_ints = void (*)(int, int);

void do_something(int l, int r) {
    cout << "l (" << l << ") + r (" << r << ") = " << l + r << endl;
}

using callback = void (*)(const std::string &);

void big_routine(int loop_count, const callback progress) {
    for (int i = 0; i < loop_count; ++i) {
        if (i % 100 == 0) {
            std::string msg("loop ");
            msg += std::to_string(i);
            progress(msg);
        }
        // routine
    }
}

void monitor(const std::string &msg) {
    cout << msg << endl;
}

void function_pointers() {
//    if (get_status > 0) { // missing () may lead to the errors in code
    if (get_status() > 0) {
        cout << "system state is invalid" << endl;
    }

    two_ints fn = do_something;
    fn(42, 99);
    cout << endl;
    big_routine(1000, monitor);
    cout << endl;
}
