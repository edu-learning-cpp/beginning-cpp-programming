//
// Created by Eduard Luhtonen on 03.08.20.
//
#include <iostream>
#include "templated_functions.h"

using std::cout, std::endl;

int maximum_bad(int lhs, int rhs) {
    return (lhs > rhs) ? lhs : rhs;
}

template<typename T>
T maximum(T lhs, T rhs) {
    return (lhs > rhs) ? lhs : rhs;
}

template<typename T, typename U>
T maximum(T lhs, U rhs) {
    return (lhs > rhs) ? lhs : rhs;
}

template<int size, typename T>
T *init(T t) {
    T *arr = new T[size];
    for (int i = 0; i < size; ++i) arr[i] = t;
    return arr;
}

template<typename T, int N>
void print_array(T (&arr)[N]) {
    for (int i = 0; i < N; ++i) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

template<typename T>
void print_array(T *arr, int size) {
    for (int i = 0; i < size; ++i) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

template<typename T, int N>
inline void print_array2(T (&arr)[N]) {
    print_array(arr, N);
}

template<typename T>
int number_of_bytes(T t) {
    return sizeof(T);
}

template<>
int number_of_bytes<const char *>(const char *t) {
    return strlen(t) + 1;
}

// template<> bool maximum<bool>(bool lhs, bool rhs) = delete;

template<typename T>
void print(T t) {
    cout << t << endl;
}

template<typename T, typename... Arguments>
void print(T first, Arguments ... next) {
    print(first);
    print(next...);
}

template<typename... Arguments>
void print(Arguments ... args) {
    int arr [sizeof...(args)] = { args... };
    for (auto i : arr) cout << i << endl;
}

template<typename... Arguments>
void print2(Arguments ... args) {
    int dummy[sizeof...(args)] = { (print(args), 0)... };
}

void templated_functions() {
    unsigned int s1 = 0xffffffff, s2 = 0x7fffffff;
    unsigned int result = maximum_bad(s1, s2);
    cout << result << endl;
    int x = maximum_bad(true, 100.99);
    cout << x << endl;
    cout << endl;

    int i = maximum(1, 100);
    double d = maximum(1.0, 100.0);
    bool b = maximum(true, false);
    cout << i << endl;
    cout << d << endl;
    cout << b << endl;
    cout << endl;

    cout << maximum(false, 100.99) << endl; // 1
    cout << maximum(100.99, false) << endl; // 100.99
    cout << maximum<int>(false, 100.99) << endl;
    cout << endl;

    int *i10 = init<10>(42);
    for (int i = 0; i < 10; ++i) cout << i10[i] << ' ';
    cout << endl;
    delete[] i10;
    cout << endl;

    int squares[] = {1, 4, 9, 16, 25};
    print_array(squares);
    print_array<int, 5>(squares);
    print_array2(squares);
    cout << endl;

    cout << number_of_bytes(5) << endl;
    cout << number_of_bytes("x") << endl;
    cout << endl;

    print(1, 2.0, "hello", true);
    cout << endl;
    print2(1, 2.0, "hello", true);
    cout << endl;
}
