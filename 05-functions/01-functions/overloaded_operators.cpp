//
// Created by Eduard Luhtonen on 03.08.20.
//
#include <iostream>
#include <algorithm>
#include <functional>
#include <vector>
#include "overloaded_operators.h"
using namespace std;

struct point {
    int x;
    int y;
};

bool operator==(const point& lhs, const point& rhs) {
    return (lhs.x == rhs.x) && (lhs.y == rhs.y);
}

bool operator!=(const point& lhs, const point& rhs) {
    return !(lhs == rhs);
}

ostream& operator<<(ostream& os, const point& pt) {
    os << "(" << pt.x << "," << pt.y << ")";
    return os;
}

// predicate function
bool equals_zero(int a) {
    return a == 0;
}

// more generic solution
template<typename T, T value>
inline bool equals(T a) {
    return a == value;
}

void overloaded_operators() {
    point p1{1, 1};
    point p2{1, 1};
    cout << boolalpha;
    cout << (p1 == p2) << endl;
    cout << (p1 != p2) << endl;
    cout << "point object is " << p1 << endl;
    cout << "point object is " << p2 << endl;
    cout << endl;

    modulus<> fn;
    cout << fn(10, 2) << endl;
    cout << fn.operator()(10, 2) << endl;
    cout << endl;

    vector<int> v1 { 1, 2, 3, 4, 5 };
    vector<int> v2(v1.size());
    fill(v2.begin(), v2.end(), 2);
    vector<int> result(v1.size());

    transform(v1.begin(), v1.end(), v2.begin(),
              result.begin(), modulus<>());

    for (int i : result) {
        cout << i << ' ';
    }
    cout << endl;

    int zeros = count_if(result.begin(), result.end(), equals_zero);
    cout << zeros << endl;
    zeros = count_if(result.begin(), result.end(), equals<int, 0>);
    cout << zeros << endl;
    //zeros = count_if(result.begin(), result.end(),  bind2nd(equal_to<>(), 0));
    cout << endl;

    // lambda expressions
    auto less_than_10 = [](int a) {return a < 10;};
    bool b = less_than_10(4);
    cout << "less than 10 = " << b << endl;
    int limit = 99;
    auto less_than = [limit](int a) { return a < limit; };
    auto x = 5;
    cout << "x is less than limit = " << less_than(x) << endl;
    cout << endl;

    auto incr = [] { static int i; return ++i; };
    incr();
    incr();
    cout << incr() << endl;

    auto swap = [](int& a, int& b) { int x = a; a = b; b = x; };
    int i = 10, j = 20;
    cout << i << " " << j << endl;
    swap(i, j);
    cout << i << " " << j << endl;
    cout << endl;

    vector<int> v { 1, 2, 3, 4, 5};
    int less_than_3 = count_if(v.begin(), v.end(), [](int a) { return a < 3; });
    cout << "There are " << less_than_3 << " items less than 3" << endl;
    cout << endl;
}
