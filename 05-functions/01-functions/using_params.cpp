//
// Created by Eduard Luhtonen on 03.08.20.
//
#include <iostream>
#include <random>
#include <initializer_list>
#include <cstdarg>
#include "using_params.h"
using std::cout, std::endl;

struct point { int x; int y; };

void set_point(point pt) {
    cout << "(" << pt.x << ", " << pt.y << ")" << endl;
}

void clear_the_screen() {
    cout << "======" << endl;
}

// using default parameters
void log_message(const std::string& msg, bool clear_screen = false){
    if (clear_screen) clear_the_screen();
    cout << msg << endl;
}

bool ask_user() {
    static auto gen = std::bind(std::uniform_int_distribution<>(0,1),std::default_random_engine());
    return gen();
}

// initializer lists
int sum(std::initializer_list<int> values) {
    int sum = 0;
    for (int i : values) sum += i;
    return sum;
}

int sum(int first, ...) {
    int sum = 0;
    va_list args;
    va_start(args, first);
    int i = first;
    while (i != -1) {
        sum += i;
        i = va_arg(args, int);
    }
    va_end(args);
    return sum;
}

int sum2(int count, ...){
    int sum = 0;
    va_list args;
    va_start(args, count);
    while(count--) {
        int i = va_arg(args, int);
        sum += i;
    }
    va_end(args);
    return sum;
}

void using_params() {
    cout << endl;
    point p{};
    p.x = 1;
    p.y = 1;
    set_point(p);
    set_point({2, 2});
    cout << endl;

    log_message("first message", true);
    log_message("second message");
    bool user_decision = ask_user();
    log_message("third message", user_decision);
    cout << endl;

    cout << sum({}) << endl;                               // 0
    cout << sum({-6, -5, -4, -3, -2, -1}) << endl;  // -21
    cout << sum({10, 20, 30}) << endl;              // 60
    cout << endl;

    cout << sum(-1) << endl;                       // 0
    cout << sum(-6, -5, -4, -3, -2, -1) << endl;   // -20 !!!
    cout << sum(10, 20, 30, -1) << endl;           // 60
    cout << endl;

    cout << sum2(0) << endl;                         // 0
    cout << sum2(6, -6, -5, -4, -3, -2, -1) << endl; // -21
    cout << sum2(3, 10, 20, 30) << endl;             // 60
    cout << endl;

    cout << sum(3, 10., 20, 30) << endl;
    cout << endl;
}
