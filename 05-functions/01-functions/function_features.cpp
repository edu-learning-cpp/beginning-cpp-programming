//
// Created by Eduard Luhtonen on 03.08.20.
//
#include <iostream>
#include <vector>
#include "function_features.h"

using std::cout, std::endl;

int factorial(int n) {
    if (n > 1) return n * factorial(n - 1);
    return 1;
}

void f(int i) {
    cout << "f(int) " << i << endl;
}

void f(double d) {
    cout << "f(double) " << d << endl;
}

// don't allow any more than 100 items
bool get_items(int count, std::vector<int> &values) {
    if (count > 100) return false;
    for (int i = 0; i < count; ++i) {
        values.push_back(i);
    }
    return true;
}

bool get_datum(/*out*/ int *pi) {
    return true;
}

bool get_data(/*in/out*/ int *psize, /*out*/ int **pi) {
    return true;
}

void function_features() {
    // recursive function
    cout << factorial(4) << endl;

    // function overloading
    f(5.0);
    f(5);
    void f(double d); // this will hide version with int parameter
    f(1); // function with double will be called
    cout << endl;

    void f(int i);
    f(1);
    f('c');
    f(1.0);
    cout << endl;

    std::vector<int> items{};
    get_items(10, items);
    for (int i : items) cout << i << ' ';
    cout << endl;
    cout << endl;

    int value = 0;
    if (get_datum(&value)) {
        cout << "value is " << value << endl;
    } else {
        cout << "cannot get the value" << endl;
    }

//    int size = 10;
//    int *buffer = nullptr;
//    if (get_data(&size, &buffer)) {
//        for (int i = 0; i < size; ++i) {
//            cout << buffer[i] << endl;
//        }
//        delete[] buffer;
//    }
    cout << endl;
}
