#include "define_functions.h"
#include "using_params.h"
#include "function_features.h"
#include "function_pointers.h"
#include "templated_functions.h"
#include "overloaded_operators.h"

int main() {
    // Defining C++ functions
    define_functions();
    // Using function parameters
    using_params();
    // Function features
    function_features();
    // Function pointers
    function_pointers();
    // Templated functions
    templated_functions();
    // Overloaded operators
    overloaded_operators();
    return 0;
}
