#include <iostream>
#include <vector>
#include <tuple>
#include <dirent.h>
#include <sys/stat.h>
#include <iomanip>

using namespace std;

using file_info = tuple<string, unsigned int>;

void files_in_folder(const char *folderPath, vector<string> &files) {
    DIR *pDir = opendir(folderPath);

    if (nullptr != pDir) {
        dirent *pInfo;
        while (nullptr != (pInfo = readdir(pDir))) {
            string findItem(folderPath);
            findItem += "/";
            findItem += pInfo->d_name;
            if (pInfo->d_type == DT_DIR) {
                // this is a folder so recurse
                if (strcmp(pInfo->d_name, ".") == 0 ||
                    strcmp(pInfo->d_name, "..") == 0) {
                    continue;
                }
                files_in_folder(findItem.c_str(), files);
            } else {
                files.emplace_back(findItem);
            }
        }
        closedir(pDir);
    }
}

int main(int argc, char *argv[]) {
    if (argc < 2) return 1;

    vector<string> files;
    files_in_folder(argv[1], files);

    vector<file_info> file_infos;
    struct stat buf{};
    for (const auto &file : files) {
        if (stat(file.c_str(), &buf) == -1) continue;
        file_infos.emplace_back(make_tuple(file, buf.st_size));
    }

    sort(file_infos.begin(), file_infos.end(),
         [](const file_info& lhs, const file_info& rhs) { return get<1>(rhs) > get<1>(lhs); });

    for (const auto& file : file_infos) {
        cout << setw(16) << get<1>(file) << " " << get<0>(file) << endl;
    }
    cout << endl;
    return 0;
}
