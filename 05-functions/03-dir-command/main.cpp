/**
 * Exercise to Chapter 5: Using functions in C++.
 * Implemented using C++ 17 std::filesystem library.
 * This implementation should be operating system agnostic.
 */
#include <iostream>
#include <filesystem>
#include <vector>

using namespace std;
namespace fs = filesystem;

using file_info = tuple<string, unsigned int>;

void read_directory(const string& name, vector<file_info> &files) {
    for (auto& p : fs::directory_iterator(name)) {
        if (p.is_directory()) {
            const auto& path = p.path();
            if (path != "." && path != "..") {
                read_directory(path, files);
            }
        } else {
            files.emplace_back(make_tuple(p.path(), p.file_size()));
        }
    }
}

int main(int argc, char* argv[]) {
    if (argc < 2) return 1;

    vector<file_info> files;
    read_directory(argv[1], files);

    sort(files.begin(), files.end(),
         [](const file_info& lhs, const file_info& rhs) { return get<1>(rhs) > get<1>(lhs); });

    for (const auto& file : files){
        cout << setw(16) << get<1>(file) << " " << get<0>(file) << endl;
    }

    return 0;
}
