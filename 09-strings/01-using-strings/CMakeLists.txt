cmake_minimum_required(VERSION 3.17)
project(01_using_strings)

set(CMAKE_CXX_STANDARD 17)

add_executable(01_using_strings main.cpp string_as_container.cpp string_as_container.h internationalisation.cpp internationalisation.h using_stream.cpp using_stream.h use_regex.cpp use_regex.h)