#include <iostream>
#include "string_as_container.h"
#include "internationalisation.h"
#include "using_stream.h"
#include "use_regex.h"

using namespace std;

void string_and_numbers() {
    cout << endl;
    double d = stod("10.5");
    d *= 4;
    cout << d << endl; // 42

    string str = "49.5 red balloons";
    size_t idx = 0;
    d = stod(str, &idx);
    d *= 2;
    string rest = str.substr(idx);
    cout << d << rest << endl; // 99 red balloons
    cout << endl;
}

int main() {
    // Using the string class as a container
    string_as_container();
    // Internationalisation
    i18n();
    // String and numbers
    string_and_numbers();
    // Using stream classes
    using_stream();
    // Using regular expressions
    use_regex();
    return 0;
}
