//
// Created by Eduard Luhtonen on 06.08.20.
//
#include <iostream>
#include <vector>
#include "string_as_container.h"

using namespace std;

void alter_string() {
    string str = "hello";
    cout << str << endl; // hello
    str.push_back('!');
    cout << str << endl; // hello!
    str.erase(0, 1);
    cout << str << endl; // ello!
    cout << endl;

    str = "hello";
    cout << str << endl;  // hello
    str.append(4, '!');
    cout << str << endl;  // hello!!!!
    str += " there";
    cout << str << endl;  // hello!!!! there
    cout << endl;

    str = "hello";
    cout << str << endl;    // hello
    str.replace(1, 1, "a");
    cout << str << endl;    // hallo
    cout << endl;

    str = "one two three";
    string str1 = str.substr(0, 3);
    cout << str1 << endl;          // one
    string str2 = str.substr(8);
    cout << str2 << endl;          // three
    cout << endl;
}

void search_strings() {
    string str = "012the678the234the890";
    string::size_type pos = 0;
    while (true) {
        pos++;
        pos = str.find("the", pos);
        if (pos == string::npos) break;
        cout << pos << " " << str.substr(pos) << endl;
    }
    cout << endl;

    pos = string::npos;
    while (true) {
        pos--;
        pos = str.rfind("the", pos);
        if (pos == string::npos) break;
        cout << pos << " " << str.substr(pos) << endl;
    }
    cout << endl;

    pos = str.find_first_of("eh");
    if (pos != string::npos) {
        cout << "found " << str[pos] << " at position ";
        cout << pos << " " << str.substr(pos) << endl;
    }

    pos = str.find_first_not_of("0123456789");
    cout << "found " << str[pos] << " at position ";
    cout << pos << " " << str.substr(pos) << endl;
    cout << endl;

    str = "  hello  ";
    cout << "|" << str << "|" << endl;  // |  hello  |
    string str1 = str.substr(str.find_first_not_of(" trn"));
    cout << "|" << str1 << "|" << endl; // |hello  |
    string str2 = str.substr(0, str.find_last_not_of(" trn") + 1);
    cout << "|" << str2 << "|" << endl; // |  hello|
    cout << endl;
}

void string_as_container() {
    string s = "hello";
    copy(s.begin(), s.end(), ostream_iterator<char>(cout));
    cout << endl;

    vector<char> v(s.begin(), s.end());
    copy(v.begin(), v.end(), ostream_iterator<char>(cout));
    cout << endl;
    cout << endl;

    // Altering strings
    alter_string();
    // Searching strings
    search_strings();
}
