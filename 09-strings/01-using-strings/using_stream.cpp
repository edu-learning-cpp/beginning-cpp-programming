//
// Created by Eduard Luhtonen on 07.08.20.
//
#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
#include "using_stream.h"

using namespace std;

struct point {
    double x = 0.0, y = 0.0;
    point() = default;
    point(double _x, double _y) : x(_x), y(_y) {}
};

ostream &operator<<(ostream &out, const point &p) {
    out << "(" << p.x << "," << p.y << ")";
    return out;
}

void using_stream() {
    auto loc = cout.getloc();
    auto f(cout.flags());
    double d = 123456789.987654321;
    cout << d << endl;
    cout << fixed;
    cout << d << endl;
    cout.precision(9);
    cout << d << endl;
    cout << scientific;
    cout << d << endl;
    cout << endl;

    d = 12.345678;
    cout << fixed;
    cout << setfill('#');
    cout << setw(15) << d << endl;
    cout << showpos << internal;
    cout << setfill('#');
    cout << setw(15) << d << endl;
    cout << endl;

    cout.flags(f);
    vector<pair<string, double>> table
            {{"one",   0},
             {"two",   0},
             {"three", 0},
             {"four",  0}};

    d = 0.1;
    for (pair<string, double> &p : table) {
        p.second = d / 17.0;
        d += 0.1;
    }

    cout << setfill(' ');
    cout << fixed << setprecision(6);
    for (const pair<string, double> &p : table) {
        cout << setw(6) << p.first << setw(10) << p.second << endl;
    }

    cout << fixed << setprecision(6) << left;
    for (const pair<string, double> &p : table) {
        cout << setw(6) << p.first << setw(10) << p.second << endl;
    }

    for (const pair<string, double> &p : table) {
        cout << setw(6) << left << p.first
             << setw(10) << right << p.second << endl;
    }
    cout << endl;

    time_t t = time(nullptr);
    tm *pt = localtime(&t);
    cout << put_time(pt, "time = %X date = %x") << endl;

    cout << put_time(pt, "month = %B day = %A") << endl;
    cout.imbue(locale("fr_CH"));
    cout << put_time(pt, "month = %B day = %A") << endl;
    cout.imbue(locale("de_CH"));
    cout << put_time(pt, "month = %B day = %A") << endl;
    cout << endl;

    cout.flags(f);
    cout << showbase;
    cout.imbue(locale("de_DE"));
    cout << "German" << endl;
    cout << put_money(109900, false) << endl;
    cout << put_money("1099", true) << endl;
    cout.imbue(locale("en_US"));
    cout << "American" << endl;
    cout << put_money(109900, false) << endl;
    cout << put_money("1099", true) << endl;
    cout << endl;

    // Converting numbers to strings using streams
    point p(10.0, -5.0);
    cout << p << endl;

    stringbuf buffer;
    ostream out(&buffer);
    out << p;
    string str = buffer.str();
    cout << str << endl;

    ostringstream os;
    os << hex;
    os << 42;
    cout << "The value is: " << os.str() << endl;
    cout << endl;

    // Reading numbers from strings using streams
    cout.flags(f);
    cout.imbue(loc);
    istringstream ss("-1.0e-6");
    double e;
    ss >> e;
    cout << e << endl;

    int i;
    ss >> i;
    string s;
    ss >> s;
    cout << "extracted " << i << " remainder " << s << endl;

    ss = istringstream("0xff");
    ss >> hex;
    ss >> i;
    cout << "extracted " << i << endl;
    cout << endl;

    ss = istringstream("Paul was born in 1942");
    int year;
    ss >> year;
    if (ss.fail()) cout << "failed to read number" << endl;

    ss = istringstream("Paul was born in 1942");
    ss >> str >> str >> str >> str;
    ss >> year;
    cout << "year: " << year << endl;

    ss = istringstream("Paul was born in 1942");
    while (!ss.eof() && !(isdigit(ss.peek()))) ss.get();
    ss >> year;
    if (!ss.fail()) cout << "the year was " << year << endl;
    cout << endl;

    str = "  hello  ";
    cout << "|" << str << "|" << endl; // |  hello  |
    ss = istringstream(str);
    ss >> ws;
    string str1;
    ss >> str1;
    cout << "|" << str1 << "|" << endl;   // |hello|
    cout << endl;

    tm indpday = {};
    str = "04.07.2017";
    ss = istringstream(str);
    ss.imbue(locale("de_CH"));
    ss >> get_time(&indpday, "%x");
    if (!ss.fail()) {
        cout.imbue(locale("en_US"));
        cout << put_time(&indpday, "%x") << endl;
    } else {
        cout << "failed" << endl;
    }
    cout << endl;
}
