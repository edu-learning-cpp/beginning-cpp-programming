//
// Created by Eduard Luhtonen on 07.08.20.
//
#include <iostream>
#include "internationalisation.h"
using namespace std;

void i18n() {
    locale loc("de_CH");
    const auto& fac = use_facet<time_put<char>>(loc);
    time_t t = time(nullptr);
    tm *td = localtime(&t);
    ostreambuf_iterator<char> it(cout);
    string format = "%x";
    fac.put(it, cout, ' ', td, &format[0], &format[0] + format.size());
    cout << endl;
}
