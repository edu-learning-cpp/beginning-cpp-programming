//
// Created by Eduard Luhtonen on 07.08.20.
//
#include <iostream>
#include <regex>
#include <iterator>
#include "use_regex.h"

using namespace std;

void use_regex() {
    regex rx("[at]"); // search for either a or t 
    cout << boolalpha;
    cout << regex_match("a", rx) << endl;  // true 
    cout << regex_match("a", rx) << endl;  // true 
    cout << regex_match("at", rx) << endl; // false
    cout << endl;

    string str("trumpet");
    rx = regex("(trump)(.*)");
    match_results<string::const_iterator> sm;
    if (regex_match(str, sm, rx)) {
        cout << "the matches were: ";
        for (unsigned i = 0; i < sm.size(); ++i) {
            cout << "[" << sm[i] << "," << sm.position(i) << "] ";
        }
        cout << endl;
    } // the matches were: [trumpet,0] [trump,0] [et,5]

    rx = regex(R"(\b\d{2}\b)");
    smatch mr;
    str = "1 4 10 42 100 999";
    string::const_iterator cit = str.cbegin();
    while (regex_search(cit, str.cend(), mr, rx)) {
        cout << mr[0] << endl;
        cit += mr.position() + mr.length();
    }

    str = "trumpet";
    rx = regex("(trump)(.*)");
    if (regex_match(str, sm, rx)) {
        string fmt = "Results: [$1] [$2]";
        cout << sm.format(fmt) << endl;
    } // Results: [trump] [et]
    cout << endl;

    // Using iterators
    str = "the cat sat on the mat in the bathroom";
    rx = regex(R"(\b(.at)([^ ]*))");
    regex_iterator<string::iterator> next(str.begin(), str.end(), rx);
    regex_iterator<string::iterator> end;

    for (; next != end; ++next) {
        cout << next->position() << " " << next->str() << ", ";
    }
    cout << endl;

    using iter = regex_token_iterator<string::iterator>;
    iter next1, end1;

    // get the text between the matches
    next1 = iter(str.begin(), str.end(), rx, -1);
    for (; next1 != end1; ++next1) cout << next1->str() << ", ";
    cout << endl;
    // the ,  ,  on the ,  in the ,

    // get the complete match
    next1 = iter(str.begin(), str.end(), rx, 0);
    for (; next1 != end1; ++next1) cout << next1->str() << ", ";
    cout << endl;
    // cat, sat, mat, bathroom,

    // get the sub match 1
    next1 = iter(str.begin(), str.end(), rx, 1);
    for (; next1 != end1; ++next1) cout << next1->str() << ", ";
    cout << endl;
    // cat, sat, mat, bat,

    // get the sub match 2
    next1 = iter(str.begin(), str.end(), rx, 2);
    for (; next1 != end1; ++next1) cout << next1->str() << ", ";
    cout << endl;
    // , , , hroom,
    cout << endl;

    // Replacing strings
    str = "use the list<int> class in the example";
    rx = regex("\\b(list)(<\\w*> )");
    string result = regex_replace(str, rx, "vector$2");
    cout << result << endl; // use the vector<int> class in the example
    cout << endl;
}
