cmake_minimum_required(VERSION 3.16)
project(01_cpp_types)

set(CMAKE_CXX_STANDARD 17)

add_executable(01_cpp_types main.cpp type_conversion.cpp type_conversion.h using_types.cpp using_types.h)