#include <iostream>
#include <cstdint>
#include <bitset>
#include <vector>
#include <tuple>
#include <iomanip>
#include "type_conversion.h"
#include "using_types.h"

using namespace std;

unsigned short reverse(unsigned short us) {
    return ((us & 0xff) << 8) | ((us & 0xff00) >> 8);
}

void integers() {
    int height = 480;
    int width = 640;
    int aspect_ration_int = width / height;
    cout << "Height: " << height << ", width: " << width << ", aspect ration: " << aspect_ration_int << endl;
    float aspect_ration = width / height;
    cout << "Height: " << height << ", width: " << width << ", aspect ration: " << aspect_ration << endl;
    float aspect_ration_f = width / (float) height;
    cout << "Height: " << height << ", width: " << width << ", aspect ration: " << aspect_ration_f << endl;
    cout << endl;

    cout << sizeof(int8_t) << endl;   // 1
    cout << sizeof(int16_t) << endl;   // 2
    cout << sizeof(int32_t) << endl;   // 4
    cout << sizeof(int64_t) << endl;   // 8
    cout << endl;

    int i = 3;
    signed s = 3;
    unsigned int ui = 3U;
    long l = 3L;
    unsigned long ul = 3UL;
    long long ll = 3LL;
    unsigned long long ull = 3ULL;
    cout << i << endl;
    cout << s << endl;
    cout << ui << endl;
    cout << l << endl;
    cout << ul << endl;
    cout << ll << endl;
    cout << ull << endl;
    cout << endl;

    unsigned long long every_other = 0xAAAAAAAAAAAAAAAA;
    unsigned long long each_other = 0x5555555555555555;
    cout << hex << showbase << uppercase;
    cout << every_other << endl;
    cout << each_other << endl;
    cout << endl;

    unsigned long long every_other2 = 0xAAAA'AAAA'AAAA'AAAA;
    int billion = 1'000'000'000;
    cout << every_other2 << endl;
    cout << dec;
    cout << billion << endl;
    cout << endl;

    unsigned long long every_other3 = 0xAAAAAAAAAAAAAAAA;
    unsigned long long each_other3 = 0x5555555555555555;
    bitset<64> bs_every(every_other3);
    bitset<64> bs_each(each_other3);
    cout << bs_every << endl;
    cout << bs_each << endl;

    bs_every.set(0);
    every_other3 = bs_every.to_ullong();
    cout << bs_every << endl;
    cout << every_other3 << endl;

    every_other3 |= 0x0000000000000001;
    cout << every_other3 << endl;
    cout << endl;

    unsigned short x = 5;
    cout << x << endl;
    cout << reverse(x) << endl;
    cout << endl;
}

void floats() {
    double one = 1.0;
    double two = 2.;
    double one_million = 1e6;
    cout << one << endl;
    cout << two << endl;
    cout << one_million << endl;

    double one2 = 0.0001e4;
    double one_billion = 1000e6;
    cout << one2 << endl;
    cout << one_billion << endl;

    float one_f = 1.f;
    // float two_f{2f}; // will raise compile error
    long double one_million2 = 1e6L;
    cout << one_f << endl;
    cout << one_million2 << endl;

    double one_billion2 = 1'000'000'000.;
    cout << one_billion2 << endl;
    cout << endl;
}

const int MAX_CHARS = 52;
const char ALPHABET[MAX_CHARS] = {
        'a', 'b', 'c', 'd', 'e', 'f', 'g',
        'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u',
        'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G',
        'H', 'I', 'J', 'K', 'L', 'M', 'N',
        'O', 'P', 'Q', 'R', 'S', 'T', 'U',
        'V', 'W', 'X', 'Y', 'Z'
};
const int MAX_DIGITS = 11;
const char DIGITS[MAX_DIGITS] = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'Q'
};

char random_char() {
    return ALPHABET[arc4random() % MAX_CHARS];
}

char random_digits() {
    return DIGITS[arc4random() % MAX_DIGITS];
}

void chars_strings() {
    char c = '~';
    cout << c << " " << (signed short) c << endl;
    c += 2;
    cout << c << " " << (signed short) c << endl;
    cout << endl;

    for (int i = 0; i < 5; i++) {
        c = random_digits();
        cout << c << endl;
        if (!isdigit(c)) break;
    }
    cout << endl;

    for (int i = 0; i < 5; i++) {
        c = random_char();
        cout << "before " << c << endl;
        if (islower(c)) c = toupper(c);
        cout << "after " << c << endl;
        if (c == 'Q') break;
    }
    cout << endl;

    char m1 = 'M';
    char m2 = '\115';
    char m3 = '\x4d';
    char m4 = 0115; // octal
    char m5 = 0x4d; // hexadecimal
    cout << m1 << endl;
    cout << m2 << endl;
    cout << m3 << endl;
    cout << m4 << endl;
    cout << m5 << endl;
    cout << endl;

    // string literals
    cout << "This is \x43\x2b\053\n";
    cout << "Mary had a little lamb,\nits fleece was white as snow."
         << endl;
    cout << "And everywhere that Mary went, "
            "the lamb was sure to go."
         << endl;
    cout << endl;

    wchar_t dollar = L'$';
    wchar_t euro = L'\u20a0';
    wcout << dollar << endl;

    cout << R"(newline is \n in C++ and "quoted text" use quotes)" << endl;
    cout << "newline is \\n in C++ and \"quoted text\" use quotes" << endl;
    cout << R"(Mary had a little lamb,
                  its fleece was white as snow)";
    cout << endl;
    cout << endl;

    string path1 = "C:\\Beginning_C++\\Chapter_03\\readme.txt";
    string path2 = R"(C:\Beginning_C++\Chapter_03\readme.txt)";
    cout << path1 << endl;
    cout << path2 << endl;
    cout << endl;
}

int use_pointer(const int *p) {
    if (p) {
        cout << "not a null pointer" << endl;
    } else {
        cout << "null pointer" << endl;
    }
    if (p != nullptr) { // preferable way to check
        cout << "not a null pointer" << endl;
    } else {
        cout << "null pointer" << endl;
    }
    return 0;
}

void booleans() {
    int *p;
    use_pointer(p);
    p = nullptr;
    use_pointer(p);
    cout << endl;
}

void print_message(void) {
    cout << "no inputs, no return value" << endl;
    cout << endl;
}

void initializers() {
    int ii = 1;
    int j = int(2);
    int k(3);
    int m{4};
    int n = {5};
    cout << ii << endl;
    cout << j << endl;
    cout << k << endl;
    cout << m << endl;
    cout << n << endl;
    cout << endl;

    vector<int> a1(42);
    cout << " size " << a1.size() << endl;
    for (int i : a1) cout << i << " ";
    cout << endl;

    vector<int> a2{42};
    cout << " size " << a2.size() << endl;
    for (int i : a2) cout << i << " ";
    cout << endl;
    cout << endl;
}

int outside;

int counter() {
    static int counter;
    return ++counter;
}

void default_values() {
    outside++; // compiler initialises this to 0
    cout << outside << endl;
    int inside;
    inside++; // compiler complains about this
    cout << inside << endl;

    cout << counter() << endl;
    cout << counter() << endl;

    int a{}; // initialised to 0
    cout << a << endl;
    cout << endl;
}

void initialisation_without_type() {
    auto i = 42;    // int
    auto l = 42l;   // long
    auto ll = 42ll;  // long long
    auto f = 1.0f;  // float
    auto d = 1.0;   // double
    auto c = 'q';   // char
    auto b = true;  // bool
    cout << i << endl;
    cout << l << endl;
    cout << ll << endl;
    cout << f << endl;
    cout << d << endl;
    cout << c << endl;
    cout << b << endl;
    cout << endl;

    vector<tuple<string, int> > beatles;
    beatles.emplace_back("John", 1940);
    beatles.emplace_back("Paul", 1942);
    beatles.emplace_back("George", 1943);
    beatles.emplace_back("Ringo", 1940);

    for (tuple<string, int> musician : beatles) {
        cout << get<0>(musician) << " " << get<1>(musician) << endl;
    }
    cout << "====" << endl;
    // same with auto
    for (auto musician : beatles) {
        cout << get<0>(musician) << " " << get<1>(musician) << endl;
    }
    cout << endl;
}

#define name_year tuple<string, int> // use of define is discouraged

void type_aliases() {
    vector<name_year > beatles;
    beatles.emplace_back("John", 1940);
    beatles.emplace_back("Paul", 1942);
    beatles.emplace_back("George", 1943);
    beatles.emplace_back("Ringo", 1940);
    for (name_year musician : beatles) {
        cout << get<0>(musician) << " " << get<1>(musician) << endl;
    }
    cout << "====" << endl;

    typedef tuple<string, int> name_year_t;
    vector<name_year_t> beatles_v;
    beatles_v.emplace_back("John", 1940);
    beatles_v.emplace_back("Paul", 1942);
    beatles_v.emplace_back("George", 1943);
    beatles_v.emplace_back("Ringo", 1940);
    for (name_year_t musician : beatles_v) {
        cout << get<0>(musician) << " " << get<1>(musician) << endl;
    }
    cout << "====" << endl;

    typedef vector<name_year_t> musician_collection_t;
    musician_collection_t beatles2;
    beatles2.emplace_back("John", 1940);
    beatles2.emplace_back("Paul", 1942);
    beatles2.emplace_back("George", 1943);
    beatles2.emplace_back("Ringo", 1940);
    for (name_year_t musician : beatles2) {
        cout << get<0>(musician) << " " << get<1>(musician) << endl;
    }

    // since C++ also possible
    using name_year2 = tuple<string, int>;
    cout << endl;
}

struct time_of_day {
    int sec;
    int min;
    int hour;
};

void print_time(time_of_day time) {
    cout << setw(2) << setfill('0') << time.hour << ":";
    cout << setw(2) << setfill('0') << time.min << ":";
    cout << setw(2) << setfill('0') << time.sec << endl;
}

struct working_hours {
    time_of_day start_work;
    time_of_day end_work;
};

void print_item_data(unsigned short item) {
    unsigned short size = (item & 0x3ff);
    const char *dirty = (item > 0x7fff) ? "yes" : "no";

    cout << "length " << size << ", ";
    cout << "is dirty: " << dirty << endl;
}

struct item_length {
    unsigned short len: 10;
    unsigned short : 5;
    bool dirty: 1;
};

struct test {
    uint8_t uc;
    uint16_t us;
    uint32_t ui;
    uint64_t ull;
};

struct test_a {
    uint8_t uc;
    alignas(uint32_t) uint16_t us;
    uint32_t ui;
    uint64_t ull;
};

struct VARIANT {
    unsigned short vt;
    union {
        unsigned char bVal;
        short iVal;
        long lVal;
        long long llVal;
        float fltVal;
        double dblVal;
    };
};

enum VARENUM {
    VT_EMPTY = 0,
    VT_NULL = 1,
    VT_UI1 = 17,
    VT_I2 = 2,
    VT_I4 = 3,
    VT_I8 = 20,
    VT_R4 = 4,
    VT_R8 = 5
};

void record_types() {
    // start work
    int start_sec = 0;
    int start_min = 30;
    int start_hour = 8;
    cout << "start work: " << start_hour << ":" << start_min << ":" << start_sec << endl;

    // end work
    int end_sec = 0;
    int end_min = 0;
    int end_hour = 17;
    cout << "end work: " << end_hour << ":" << end_min << ":" << end_sec << endl;

    time_of_day start_work;
    start_work.sec = 0;
    start_work.min = 30;
    start_work.hour = 8;

    time_of_day end_work;
    end_work.sec = 0;
    end_work.min = 0;
    end_work.hour = 17;

    cout << "start: ";
    print_time(start_work);
    cout << "end: ";
    print_time(end_work);
    cout << endl;

    // initialising
    time_of_day lunch{0, 0, 13};
    time_of_day midnight{};
    time_of_day midnight_30{0, 30};
    cout << "lunch: ";
    print_time(lunch);
    cout << "midnight: ";
    print_time(midnight);
    cout << "midnight 30: ";
    print_time(midnight_30);
    cout << endl;

    working_hours weekday{{0, 30, 8},
                          {0, 0,  17}};
    cout << "weekday:" << endl;
    print_time(weekday.start_work);
    print_time(weekday.end_work);
    cout << endl;

    print_item_data(5);
    cout << endl;

    unsigned short us = 5;
    item_length slen;
    slen.len = us & 0x3ff;
    slen.dirty = us > 0x7fff;
    cout << "length " << slen.len << ", ";
    cout << "is dirty: " << slen.dirty << endl;
    cout << endl;

    // alignment
    cout << "alignment boundary for int is "
         << alignof(int) << endl;                     // 4
    cout << "alignment boundary for double is "
         << alignof(double) << endl;                  // 8
    cout << "alignment boundary for time_of_day is "
         << alignof(time_of_day) << endl;             // 4
    cout << "alignment boundary for test is "
         << alignof(test) << endl;
    cout << "alignment boundary for test_a is "
         << alignof(test_a) << endl;
    cout << endl;

    // union
    // pseudo code, real VARIANT should not be handled like this
    VARIANT var{}; // clear all items
    var.vt = VT_I4; // specify the type
    var.lVal = 42;  // set the appropriate member
    cout << var.vt << endl;
    cout << var.lVal << endl;
    cout << endl;

    union d_or_i {
        double d;
        long long i;
    };
    d_or_i test;
    test.i = 42;
    cout << test.i << endl; // correct use
    cout << test.d << endl; // nonsense printed
    cout << endl;
}

void runtime_type_info() {
    cout << "int type name: " << typeid(int).name() << endl;
    int i = 42;
    cout << "i type name: " << typeid(i).name() << endl;

    auto a = i;
    if (typeid(a) == typeid(int)) {
        cout << "we can treat a as an int" << endl;
    }
    cout << endl;

    // Determining type limits
    cout << "The int type can have values between ";
    cout << numeric_limits<int>::min() << " and  ";
    cout << numeric_limits<int>::max() << endl;
    cout << endl;
}

int main(int argc, char* argv[]) {
    integers();
    floats();
    chars_strings();
    booleans();
    // void
    print_message();
    initializers();
    default_values();
    initialisation_without_type();
    type_aliases();
    record_types();
    runtime_type_info();

    // Converting between types
    type_conversion();

    // Using C++ types
    return using_types(argc, argv);
}
