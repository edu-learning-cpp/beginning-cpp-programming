//
// Created by Eduard Luhtonen on 28.07.20.
//
#include <iostream>
#include "type_conversion.h"

using namespace std;

void f(int i) {
    cout << i << endl;
}

void unsafe_d(void *pData) {
    auto *pd = static_cast<double *>(pData);
    cout << *pd << endl;
}

void type_conversion() {
    short s = 42;
    f(s); // s is promoted to int

    bool b = true;
    f(b); // b is promoted to int

    int i = 0.0; // narrowing
    f(i);

    int x = 0;
    if (x = 1) cout << "not zero" << endl; // used assignment instead of equality
    else cout << "is zero" << endl;

    // converting signed types
    int v = -3;
    unsigned int u = v;
    cout << v << endl;
    cout << u << endl;

    if (u < v) // C4018
        cout << "u is smaller than v" << endl;
    cout << endl;

    // Casting
    // casting away const-ness
    const char *ptr = "0123456";
    cout << ptr << endl;
    char *pWriteable = const_cast<char *>(ptr);
    // pWriteable[3] = '\0'; // may cause runtime error
    cout << endl;

    // casting without runtime checks
    double pi = 3.1415;
    int pi_whole = static_cast<int>(pi);
    cout << pi << endl;
    cout << pi_whole << endl;

    unsafe_d(&pi);       // works as expected
    unsafe_d(&pi_whole); // oops!
    cout << endl;

    // Casting with list initializer
    char c = 35;
    cout << c << endl;
    cout << static_cast<short>(c) << endl;
    cout << short{ c } << endl;
    cout << endl;

    // Using C casts
    float f1 = (float)pi;
    float f2 = float(pi);
    cout << f1 << endl;
    cout << f2 << endl;
    cout << endl;
}
