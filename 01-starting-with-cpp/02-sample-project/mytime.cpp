//
// Created by Eduard Luhtonen on 24.07.20.
//
#include "utils.h"
#include "mytime.h"

void print_time() {
    std::time_t now = std::time(nullptr);
    std::cout << ", the time and date are " << std::ctime(&now) << std::endl;
}
