//
// Created by Eduard Luhtonen on 24.07.20.
//
#include "utils.h"
#include "name.h"

void print_name() {
    std::cout << "Please type your first name and press [Enter] ";
    std::string name;
    std::cin >> name;
    std::cout << name;
}
