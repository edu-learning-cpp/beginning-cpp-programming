cmake_minimum_required(VERSION 3.16)
project(01_cpp_projects)

set(CMAKE_CXX_STANDARD 17)

add_executable(01_cpp_projects main.cpp)