#include <iostream>

// global constant
#define NUMBER 4

// symbols
#ifdef DEBUG
// macros
// use backslash as line-continuation to have multiline macros
#define MESSAGE(c, v) \
for(int i = 1; i < c; ++i) std::cout << v[i] << std::endl;
#else
#define MESSAGE
#endif

int main(int argc, char * argv[]) {
    // passing parameters
    std::cout << "there are " << argc << " parameters" << std::endl;
    for (int i = 0; i < argc; ++i) {
        std::cout << argv[i] << std::endl;
    }

    std::cout << std::endl;
    std::cout << NUMBER << std::endl;
    std::cout << std::endl;

    MESSAGE(argc, argv);
    std::cout << "invoked with " << argv[0] << std::endl;

    return 0;
}
