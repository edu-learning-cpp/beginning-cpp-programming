//
// Created by Eduard Luhtonen on 05.08.20.
//
#include <iostream>
#include "inheritance_and_composition.h"
using namespace std;

int open_file(const string &basicString) {
    return 0;
}
void close_file(int handle) {}

// Inheriting from a class
class os_file {
    const string file_name;
    int file_handle;
    // other data members
public:
    os_file(const string& name) : file_name(name), file_handle(open_file(name)) {}
    ~os_file() { close_file(file_handle); }
    long get_size_in_bytes();
    // other methods
};

class mp3_file : public os_file {
    long length_in_secs;
    // other data members
public:
    mp3_file(const string& name) : os_file(name) {}
    ~mp3_file() { /* clean up mp3 stuff*/ }
    long get_length_in_seconds();
    // other methods
};

// overriding methods and hiding names
struct base {
    void f(){
        cout << "f() in base class" << endl;
    }
    void g(){ /* do something */ }
};

struct derived : base {
    void f() {
        base::f();
        cout << "f() in derived class" << endl;
        // do more stuff
    }
};

struct derived_two : base {
    void f(int i) {
        base::f();
        cout << "f(" << i << ") in derived_two class " << endl;
        // do more stuff
    }
};

class base_c {
protected:
    void test() {
        cout << "base class test()" << endl;
    }
    void f() {
        cout << "base class f()" << endl;
    }
public:
    void g() {
        cout << "base class g()" << endl;
    }
};

class derived_c : public base_c {
protected:
    void g() ;
public:
    void f() { test(); }
};

// Multiple inheritance
class base1 {
public:
    int x = 1;
    void a(int i) {
        cout << "a(" << i << ")" << endl;
    }
};
class base2 {
public:
    int x = 2;
    void a() {
        cout << "a()" << endl;
    }
};
class derived12 : public base1, public base2 {};

void object_slicing() {
    derived d;
    base b1 = d; // slicing through the copy constructor
    base b2;
    b2 = d;      // slicing through assignment
}

void inheritance_and_composition() {
    derived d;
    d.f(); // calls derived::f
    d.g(); // calls base::g

    derived_two d2;
    d2.f(42); // OK
    // d2.f();   // won't compile, derived::f(int) hides base::f

    d2.derived_two::f(42); // same call as above
    d2.base::f();            // call base class method
    derived_two *p = &d2;   // get an object pointer
    p->base::f();           // call base class method
    // delete p;
    cout << endl;

    // protected
    base_c bc;
    // bc.test();  // won't compile
    derived_c dc;
    dc.f();     // OK
    //dc.test();  // won't compile
    cout << endl;

    derived12 d12;
    cout << d12.base1::x << endl; // the base1 version
    cout << d12.base2::x << endl; // the base2 version
    // cout << d12.x << endl; // won't compile

    // d12.a();          // should be a from base2, compiler still complains
    d12.base1::a(42); // the base1 version
    d12.base2::a();   // the base2 version

    // object slicing
    object_slicing();
    cout << endl;
}
