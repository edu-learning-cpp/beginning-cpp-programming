#include "inheritance_and_composition.h"
#include "intro_to_polymorphism.h"

int main() {
    // Inheritance and composition
    inheritance_and_composition();
    // Introducing polymorphism
    intro_to_polymorphism();
    return 0;
}
