cmake_minimum_required(VERSION 3.17)
project(01_oop_intro)

set(CMAKE_CXX_STANDARD 17)

add_executable(01_oop_intro main.cpp inheritance_and_composition.cpp inheritance_and_composition.h intro_to_polymorphism.cpp intro_to_polymorphism.h)