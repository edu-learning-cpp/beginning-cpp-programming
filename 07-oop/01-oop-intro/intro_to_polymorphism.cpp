//
// Created by Eduard Luhtonen on 05.08.20.
//
#include <iostream>
#include <vector>
#include "intro_to_polymorphism.h"
using namespace std;

class base {
    /*members*/
    int x = 0;
protected:
    virtual void output(ostream& stm) const { stm << x << " "; }
public:
    virtual void who() { cout << "base "; }
    friend ostream& operator<<(ostream& stm, const base& b) {
        // thru b we can access the base private/protected members
        stm << "base: " << b.x << " ";
        return stm;
    }
};
class derived1 : public base {
    /*members*/
public:
    void who() { cout << "derived1 "; }
};
class derived2 : public base {
    /*members*/
public:
    void who() { cout << "derived2 "; }
};
class derived3 : public base {
    /*members*/
public:
    void who() { cout << "derived3 "; }
};

void who_is_it(base& p) {
    p.who();
}

class derived : public base {
    int y = 0;
protected:
    virtual void output(ostream& stm) const {
        base::output(stm);
        stm << y << " ";
    }
};

struct abstract_base {
    virtual void f() = 0;
    void g() {
        cout << "do something" << endl;
        f();
    }
};

void call_it(abstract_base& r) {
    r.g();
}

void call_it2(abstract_base& r) {
    r.f();
}

struct derived11 : abstract_base {
    virtual void f() override { cout << "derived11::f" << endl; }
};

struct derived12 : abstract_base {
    virtual void f() override { cout << "derived12::f" << endl; }
};

namespace smart_pointers {
    struct base {
        virtual ~base() {}
        virtual void who() = 0;
    };

    struct derived : base {
        virtual void who() { cout << "derived"; }
    };

    struct base1 {
        virtual ~base1() {}
        virtual void who() = 0;
    };

    struct base2 {
        virtual ~base2() {}
        virtual void what() = 0;
    };

    struct derived1 : base1, base2 {
        virtual void who()  { cout << "derived "; }
        virtual void what() { cout << "derived "; }
    };

    void smart_pointers() {
        // both of these are acceptable
        shared_ptr<base> b_ptr1(new derived);
        shared_ptr<base> b_ptr2 = make_shared<derived>();
        shared_ptr<base> b_ptr(new derived);
        b_ptr->who(); // prints "derived"
        cout << endl;

        shared_ptr<derived1> d_ptr(new derived1);
        d_ptr->who();
        d_ptr->what();

        base1 *b1_ptr = d_ptr.get();
        b1_ptr->who();
        base2 *b2_ptr = dynamic_cast<base2*>(b1_ptr);
        b2_ptr->what();
        cout << endl;
    }
}

namespace interfaces {
#define interface struct

    interface IPrint {
    public:
        virtual void set_page(/*size, orientation etc*/) = 0;
        virtual void print_page(const string &str) = 0;
    };

    interface IScan {
    public:
        virtual void set_page(/*resolution etc*/) = 0;
        virtual string scan_page() = 0;
    };

    interface IPrint2 : IPrint {
        virtual void print_doc(const vector<string> &doc) = 0;
    };

    class inkjet_printer : public IPrint2, public IScan {
    public:
        virtual void set_page(/*size, orientation etc*/) override {
            // set page properties
        }
        virtual void print_page(const string &str) override {
            cout << str << endl;
        }
        virtual void print_doc(const vector<string> &doc) override {
            /* code*/
        }
        virtual string scan_page() override {
            static int page_no;
            string str("page ");
            str += to_string(++page_no);
            return str;
        }
    };

    void print_doc(IPrint *printer, vector<string> doc) {
        for (auto d : doc) {
            printer->print_page(d);
        }
    }

    void scan_doc(IScan *scanner, int num_pages) {
        for (int i = 0; i < num_pages; ++i) {
            cout << "page scanned: " << scanner->scan_page() << endl;
        }
    }

    void interfaces() {
        inkjet_printer inkjet;
        IPrint *printer = &inkjet;
        printer->set_page(/*properties*/);
        vector<string> doc {"page 1", "page 2", "page 3"};
        print_doc(printer, doc);
        cout << endl;

        IScan *scanner = &inkjet;
        scanner->set_page(/*properties*/);
        scan_doc(scanner, 5);
        cout << endl;

        IPrint *printer1 = dynamic_cast<IPrint*>(scanner);
        if (printer1 != nullptr){
            printer1->set_page(/*properties*/);
            vector<string> doc1 {"page 1", "page 2", "page 3"};
            print_doc(printer1, doc1);
        }
        cout << endl;
    }
}

namespace mixin_classes {
    // Library code
    template <typename BASE>
    class mixin : public BASE {
    public:
        void something() {
            cout << "mixin do something" << endl;
            BASE::something();
            cout << "mixin something else" << endl;
        }
    };

    // Client code to adapt the mixin class
    class impl {
    public:
        void something() {
            cout << "impl do something" << endl;
        }
    };

    template <typename BASE>
    class mixin2 : public BASE
    {
    public:
        void something()
        {
            cout << "mixin2 do something" << endl;
            BASE::something();
            cout << "mixin2 something else" << endl;
        }
    };

    void mixin_classes() {
        mixin<impl> obj;
        obj.something();
        cout << endl;

        mixin2< mixin<impl> > obj2;
        obj2.something();

//        obj2.impl::init(/* parameters */);  // call impl::init
//        obj2.mixin::init(/* parameters */); // call mixin::init
//        obj2.init(/* parameters */);        // call mixin2::init
    }
}

void intro_to_polymorphism() {
    derived1 d1;
    who_is_it(d1);
    derived2 d2;
    who_is_it(d2);
    derived3 d3;
    who_is_it(d3);
    cout << endl;

    base *arr[] = { &d1, &d2, &d3 };
    for (auto p : arr) p->who();
    cout << endl;

    vector<base> vec = { d1, d2, d3 };
    for (auto b : vec) b.who();
    cout << endl;

    vector<reference_wrapper<base> > vec_w = { d1, d2, d3 };
    for (auto b : vec_w) b.get().who();
    cout << endl;
    cout << endl;

    derived11 d11;
    call_it(d11);
    cout << endl;

    string str = "hello";
    const type_info& ti = typeid(str);
    cout << ti.name() << endl;
    cout << endl;

    smart_pointers::smart_pointers();
    cout << endl;

    interfaces::interfaces();

    mixin_classes::mixin_classes();
    cout << endl;
}
