//
// Created by Eduard Luhtonen on 06.08.20.
//
#include <iostream>
#include <vector>
#include <list>
#include <forward_list>
#include <map>
#include <set>
#include <queue>
#include "containers.h"

using namespace std;

struct point {
    double x = 0, y = 0;
    point(double _x, double _y) : x(_x), y(_y) {}
};

ostream& operator<<(ostream& os, const point& pt) {
    os << "(" << pt.x << "," << pt.y << ")";
    return os;
}

template <typename container>
void print(container& items) {
    for (auto item : items) {
        cout << item << " ";
    }
    cout << endl;
}

void lists() {
    list<int> primes_list{ 3,5,7 };
    primes_list.push_back(11);
    primes_list.push_back(13);
    primes_list.push_front(2);
    primes_list.push_front(1);
    print(primes_list);

    int last = primes_list.back(); // get the last item
    primes_list.pop_back();        // remove it
    cout << "last: " << last << endl;
    print(primes_list);

    auto start = primes_list.begin();   // 1
    start++;                            // 2
    auto lst = start;                   // 2
    lst++;                              // 3
    lst++;                              // 5
    primes_list.erase(start, lst);      // remove 2 and 3
    print(primes_list);
    cout << endl;

    list<int> planck{ 6,6,2,6,0,7,0,0,4,0 };
    print(planck);
    planck.remove(6);            // {2,0,7,0,0,4,0}
    print(planck);

    auto it = planck.begin();
    ++it;
    ++it;
    planck.insert(it, -1); // {6,6,-1,2,6,0,7,0,0,4,0}
    print(planck);

    list<point> points;
    point p(1.0, 1.0);
    points.push_back(p);
    points.emplace_back(2.0, 2.0);
    print(points);
    cout << endl;

    list<int> num1 { 2,7,1,8,2,8 }; // digits of Euler's number
    list<int> num2 { 3,1,4,5,6,8 }; // digits of pi
    cout << "num1: ";
    print(num1);
    cout << "num2: ";
    print(num2);
    num1.swap(num2);
    cout << "after swap" << endl;
    cout << "num1: ";
    print(num1);
    cout << "num2: ";
    print(num2);
    cout << endl;

    num1.swap(num2);
    num1.sort();
    cout << "num1 sorted: ";
    print(num1);
    num2.sort();
    cout << "num2 sorted: ";
    print(num2);
    num1.merge(num2);
    cout << "num1 merged with num2: ";
    print(num1);
    num1.unique();
    cout << "num1 unique: ";
    print(num1);
    cout << endl;
}

void forward_lists() {
    forward_list<int> euler { 2,7,1,8,2,8 };
    euler.push_front(-1);       // { -1,2,7,1,8,2,8 }
    auto it = euler.begin();    // iterator points to -1
    euler.insert_after(it, -2); // { -1,-2,2,7,1,8,2,8 }
    euler.pop_front();          // { -2,2,7,1,8,2,8 }
    euler.remove_if([](int i){return i < 0;}); // { 2,7,1,8,2,8 }
}

void vectors() {
    vector<int> distrib(10); // ten intervals
    for (int count = 0; count < 1000; ++count) {
        int val = rand() % 10;
        ++distrib[val];
    }
    for (int i : distrib) cout << i << " ";
    cout << endl;
    cout << endl;
}

void maps() {
    map<string, int> people;
    people.emplace("Washington", 1789);
    people.emplace("Adams", 1797);
    people.emplace("Jefferson", 1801);
    people.emplace("Madison", 1809);
    people.emplace("Monroe", 1817);

    auto it = people.begin();
    pair<string, int> first_item = *it;
    cout << first_item.first << " " << first_item.second << endl;

    auto result = people.emplace("Adams", 1825);
    if (!result.second)
        cout << (*result.first).first << " already in map" << endl;

    result = people.emplace("Monroe", 1817);
    people.emplace_hint(result.first, "Polk", 1845);
    for (pair<string, int> p : people) {
        cout << p.first << " " << p.second << endl;
    }
    cout << endl;
}

void sets() {
    set<string> people{
            "Washington","Adams", "Jefferson","Madison","Monroe",
            "Adams", "Van Buren","Harrison","Tyler","Polk"};
    for (const string& s : people) cout << s << endl;
    cout << endl;
}

struct task{
    string name;
    int priority;
    task(const string& n, int p) : name(n), priority(p) {}
    bool operator <(const task& rhs) const {
        return this->priority < rhs.priority;
    }
};

void special_containers() {
    queue<int> primes;
    primes.push(1);
    primes.push(2);
    primes.push(3);
    primes.push(5);
    primes.push(7);
    primes.push(11);
    while (primes.size() > 0) {
        cout << primes.front() << ",";
        primes.pop();
    }
    cout << endl; // prints 1,2,3,5,7,11

    priority_queue<task> to_do;
    to_do.push(task("tidy desk", 1));
    to_do.push(task("check in code", 10));
    to_do.push(task("write spec", 8));
    to_do.push(task("strategy meeting", 8));

    while (to_do.size() > 0) {
        cout << to_do.top().name << " " << to_do.top().priority << endl;
        to_do.pop();
    }
    cout << endl;
}

void iterators() {
    vector<int> primes { 1,2,3,5,7,11,13 };
    const auto it = primes.begin(); // const has no effect
    *it = 42;
    auto cit = primes.cbegin();
    //*cit = 1;                       // will not compile
    for (auto p : primes) {
        cout << p << " ";
    }
    cout << endl;

    vector<int> primes1 { 1,2,3,5,7,11,13 };
    auto rit = primes1.rbegin();
    while (rit != primes1.rend()) {
        cout << *rit++ << " ";
    }
    cout << endl; // prints 13,11,7,5,4,3,2,1
    cout << endl;

    // Input and output iterators
    vector<int> data { 1,2,3,4,5 };
    vector<int> results;
    results.resize(data.size());
    transform(
            data.begin(), data.end(),
            results.begin(),
            [](int x){ return x*x; } );
    for (auto i : results) {
        cout << i << " ";
    }
    cout << endl;

    vector<int> vec{ 1,2,3,4,5 };
    vec.resize(vec.size() * 2);
    transform(vec.begin(), vec.begin() + 5,
              vec.begin() + 5, [](int i) { return i*i; });
    for (auto i : vec) {
        cout << i << " ";
    }
    cout << endl;
    cout << endl;

    vector<int> results_b;
    transform(
            data.begin(), data.end(),
            back_inserter(results_b),
            [](int x){ return x*x; } ); // 1,4,9,16,25
    for (auto i : results_b) {
        cout << i << " ";
    }
    cout << endl;

    vector<int> results_b1;
    transform(
            data.rbegin(), data.rend(),
            back_inserter(results_b1),
            [](int x){ return x*x; } ); // 25,16,9,4,1
    for (auto i : results_b1) {
        cout << i << " ";
    }
    cout << endl;
    cout << endl;

    // Using iterators with the C Standard Library
    // do some calculations and fill the list
    vector<int> temp(data.begin(), data.end());
    size_t size = temp.size(); // can pass size to a C function
    int *p = &temp[0];         // can pass p to a C function
    cout << "first element: " << *p << ", size: " << size << endl;
    cout << endl;
}

void stream_iterators() {
    vector<int> data { 1,2,3,4,5 };
    for (int i : data) cout << i << " ";
    cout << endl;

    ostream_iterator<int> my_out(cout, " ");
    copy(data.cbegin(), data.cend(), my_out);
    cout << endl;
    cout << endl;
}

void containers() {
    vector<int> primes{1, 3, 5, 7, 11, 13};
    for (size_t idx = 0; idx < primes.size(); ++idx) {
        cout << primes[idx] << " ";
    }
    cout << endl;

    // List
    lists();
    // Forward list
    forward_lists();
    // Vector
    vectors();
    // Maps and multimaps
    maps();
    // Sets and multisets
    sets();
    // Special purpose containers
    special_containers();
    // Using iterators
    iterators();
    // Stream iterators
    stream_iterators();
}
