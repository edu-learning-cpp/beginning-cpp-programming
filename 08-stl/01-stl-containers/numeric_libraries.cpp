//
// Created by Eduard Luhtonen on 06.08.20.
//
#include <iostream>
#include <ratio>
#include <complex>
#include <cmath>
#include "numeric_libraries.h"
using namespace std;

template<typename units>
class dist_units {
    double data;
public:
    dist_units(double d) : data(d) {}

    template <class other>
    dist_units(const dist_units<other>& len) : data(len.value() *
                                                    ratio_divide<units, other>::type::den /
                                                    ratio_divide<units, other>::type::num) {}

    double value() const { return data; }
};

void numeric_libraries() {
    ratio<15, 20> ratio;
    cout << ratio.num << "/" << ratio.den << endl;

    ratio_add<std::ratio<27, 11>, std::ratio<5, 17>> ratioa;
    cout << ratioa.num << "/" << ratioa.den << endl;

    using sum = ratio_add<std::ratio<27, 11>, std::ratio<5, 17>>;
    cout << sum::num << "/" << sum::den << endl;

    bool result = ratio_greater<sum, std::ratio<25, 19>>::value;
    cout << boolalpha << result << endl;

    using invalid = std::ratio<1, 0>;
    // cout << invalid::num << "/" << invalid::den << endl; // will not compile because of division by zero
    cout << endl;

    double radius_nm = 10.0;
    double volume_nm = pow(radius_nm, 3) * 3.1415 * 4.0 / 3.0;
    cout << "for " << radius_nm << "nm "
                                   "the volume is " << volume_nm << "nm3" << endl;
    double factor = ((double)nano::num / nano::den);
    double vol_factor = pow(factor, 3);
    cout << "for " << radius_nm * factor << "m the volume is " << volume_nm * vol_factor << "m3" << endl;
    cout << endl;

    dist_units<kilo> earth_diameter_km(12742);
    cout << earth_diameter_km.value() << "km" << endl;
    dist_units<std::ratio<1>> in_meters(earth_diameter_km);
    cout << in_meters.value()<< "m" << endl;
    dist_units<std::ratio<1609344, 1000>> in_miles(earth_diameter_km);
    cout << in_miles.value()<< "miles" << endl;
    cout << endl;

    // complex numbers
    complex<double> a(1.0, 1.0);
    complex<double> b(-0.5, 0.5);
    complex<double> c = a + b;
    cout << a << " + " << b << " = " << c << endl;
    complex<double> d = polar(1.41421, -3.14152 / 4);
    cout << d << endl;
    cout << endl;
}
