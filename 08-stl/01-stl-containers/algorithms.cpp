//
// Created by Eduard Luhtonen on 06.08.20.
//
#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include <random>
#include "algorithms.h"
using namespace std;

template <typename T>
ostream& operator<<(ostream& os, vector<T>& vec) {
    os << "{ ";
    for (auto it : vec) {
        os << it << " ";
    }
    os << "}";
    return os;
}

void item_iteration() {
    vector<int> vec;
    vec.resize(5);
    fill(vec.begin(), vec.end(), 42);
    cout << vec << endl;

    vector<int> vec1(5);
    generate(vec1.begin(), vec1.end(),
             []() {static int i; return ++i; });
    cout << vec1 << endl;

    vector<int> vec2 { 1,4,9,16,25 };
    for_each(vec2.begin(), vec2.end(),
             [](int i) { cout << i << " "; });
    cout << endl;

    vector<int> vec3 { 1,2,3,4,5 };
    for_each(vec3.begin(), vec3.end(),
             [](int& i) { i *= i; });
    cout << vec3 << endl;

    vector<int> results;
    for_each(vec1.begin(), vec1.end(),
             [&results](int i) { results.push_back(i*i); });
    cout << results << endl;

    vector<int> vec4 { 1,2,3,4,5 };
    vector<int> vec5 { 5,4,3,2,1 };
    vector<int> results1;
    transform(vec4.begin(), vec4.end(), vec5.begin(),
              back_inserter(results1), [](int i, int j) { return i*j; });
    cout << results1 << endl;
    cout << endl;
}

void get_info() {
    vector<int> planck{ 6,6,2,6,0,7,0,0,4,0 };
    auto number = count(planck.begin(), planck.end(), 6);
    cout << "count: " << number << endl;
    cout << endl;
}

void compare_containers() {
    vector<int> v1 { 1,2,3,4 };
    vector<int> v2 { 1,2 };
    vector<int> v3 { 5,6,7 };
    cout << boolalpha;
    cout << (v1 > v2) << endl; // true
    cout << (v1 > v3) << endl; // false
    cout << endl;
}

void changing_items() {
    vector<int> planck{ 6,6,2,6,0,7,0,0,4,0 };
    vector<int> result(4);          // we want 4 items
    auto it1 = planck.begin();      // get the first position
    it1 += 2;                       // move forward 2 places
    auto it2 = it1 + 4;             // move 4 items
    move(it1, it2, result.begin()); // {2,6,0,7}
    cout << planck << endl;
    cout << result << endl;

    vector<int> result1;
    remove_copy(planck.begin(), planck.end(),
                back_inserter(result1), 6);
    cout << result1 << endl;
    cout << planck << endl;

    auto new_end = remove(planck.begin(), planck.end(), 6); // {2,0,7,0,0,4,0,0,4,0}
    planck.erase(new_end, planck.end()); // {2,0,7,0,0,4,0}
    cout << planck << endl;

    planck = vector<int>{ 6,6,2,6,0,7,0,0,4,0 };
    auto it = planck.begin();
    it += 4;
    rotate(planck.begin(), it, planck.end());
    cout << planck << endl;

    planck = vector<int> { 6,6,2,6,0,7,0,0,4,0 };
    vector<int> temp;
    unique_copy(planck.begin(), planck.end(), back_inserter(temp));
    planck.assign(temp.begin(), temp.end());
    cout << planck << endl;
    cout << endl;
}

void find_items() {
    vector<int> planck{ 6,6,2,6,0,7,0,0,4,0 };
    auto imin = min_element(planck.begin(), planck.end());
    auto imax = max_element(planck.begin(), planck.end());
    cout << "values between " << *imin << " and "<< *imax << endl;

    vector<int> vec{0,1,2,3,4,4,5,6,7,7,7,8,9};
    vector<int>::iterator it = vec.begin();

    do {
        it = adjacent_find(it, vec.end());
        if (it != vec.end()) {
            cout << "duplicate " << *it << endl;
            ++it;
        }
    } while (it != vec.end());
    cout << endl;
}

void sort_items() {
    vector<int> vec{45,23,67,6,29,44,90,3,64,18};
    auto middle = vec.begin() + 5;
    partial_sort(vec.begin(), middle, vec.end());
    cout << "smallest items" << endl;
    for_each(vec.begin(), middle, [](int i) {cout << i << " "; });
    cout << endl; // 3 6 18 23 29
    cout << "biggest items" << endl;
    for_each(middle, vec.end(), [](int i) {cout << i << " "; });
    cout << endl; // 67 90 45 64 44

    vector<int> vec1;
    for (int i = 0; i < 10; ++i) vec1.push_back(i);
    cout << "before shuffle: " << vec1 << endl;
    random_device rd;
    shuffle(vec1.begin(), vec1.end(), rd);
    cout << "after shuffle: " << vec1 << endl;
    cout << endl;
}

void algorithms() {
    // Iteration of items
    item_iteration();
    // Getting information
    get_info();
    // Comparing containers
    compare_containers();
    // Changing Items
    changing_items();
    // Finding items
    find_items();
    // Sorting items
    sort_items();
}
