//
// Created by Eduard Luhtonen on 06.08.20.
//
#include <iostream>
#include <utility>
#include "pair_and_tuple.h"
using namespace std;

template <typename T1, typename T2>
ostream& operator<<(ostream& os, const pair<T1, T2>& p) {
    os << "{" << p.first << "," << p.second << "}";
    return os;
}

void tuples() {
    tuple<int, int, int> t3 { 1,2,3 };
    cout << "{"
         << get<0>(t3) << "," << get<1>(t3) << "," << get<2>(t3)
         << "}" << endl; // {1,2,3}

    int& tmp = get<0>(t3);
    tmp = 42;
    get<1>(t3) = 99;
    cout << tmp << endl;
    cout << endl;

    int i1, i2, i3;
    tie(i1, i2, i3) = t3;
    cout << i1 << "," << i2 << "," << i3 << endl;

    tuple<int&, int&, int&> tr3 = tie(i1, i2, i3);
    cout << "{"
         << get<0>(tr3) << "," << get<1>(tr3) << "," << get<2>(tr3)
         << "}" << endl;
    tr3 = t3;
    cout << "{"
         << get<0>(tr3) << "," << get<1>(tr3) << "," << get<2>(tr3)
         << "}" << endl;
}

void pair_and_tuple() {
    // pairs
    auto name_age = make_pair("Edu", 52);
    cout << name_age << endl;

    pair <int, int> a(1, 1);
    pair <int, int> b(1, 2);
    cout << boolalpha;
    cout << a << " < " << b << " " << (a < b) << endl;
    cout << endl;

    int i1 = 0, i2 = 0;
    pair<int&, int&> p(i1, i2);
    cout << p << endl;
    ++p.first; // changes i1
    cout << i1 << ", " << i2 << endl;

    auto p2 = make_pair(ref(i1), ref(i2));
    ++p2.first; // changes i1
    cout << i1 << ", " << i2 << endl;
    cout << endl;

    auto p3 = minmax(20,10);
    cout << "{" << p3.first << "," << p3.second << "}" << endl;
    cout << endl;

    tuples();
    cout << endl;
}
