cmake_minimum_required(VERSION 3.17)
project(01_stl_containers)

set(CMAKE_CXX_STANDARD 17)

add_executable(01_stl_containers main.cpp pair_and_tuple.cpp pair_and_tuple.h containers.cpp containers.h algorithms.cpp algorithms.h numeric_libraries.cpp numeric_libraries.h)