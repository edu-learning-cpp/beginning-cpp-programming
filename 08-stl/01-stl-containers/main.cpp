#include "pair_and_tuple.h"
#include "containers.h"
#include "algorithms.h"
#include "numeric_libraries.h"

int main() {
    // Working with pairs and tuples
    pair_and_tuple();
    // Containers
    containers();
    // Algorithms
    algorithms();
    // Using the numeric libraries
    numeric_libraries();
    return 0;
}
